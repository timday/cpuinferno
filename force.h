#ifndef _force_h_
#define _force_h_

#include <cstddef>
#include <cstdint>

extern void force(char);
extern void force(int);
extern void force(size_t);
extern void force(int64_t);
extern void force(double);

#endif
