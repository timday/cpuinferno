#!/usr/bin/python

ref='results/ec2-hi1.4xlarge-hvm-E5620.txt'

order=[line[1:].split(':')[0].strip() for line in open(ref) if len(line)>0 and line[0]=='$']

def results(filename):
    with open(filename) as file:
        return {line[1:].split(':')[0].strip() : line.split(':')[1].strip().split(' ')[0] for line in file if len(line)>0 and line[0]=='$'}

sandy=results('results/ec2-c3.4xlarge-hvm-E5-2680.txt')
westmere=results('results/ec2-hi1.4xlarge-hvm-E5620.txt')
nehalem=results('results/ec2-cc1.4xlarge-hvm-X5570.txt')

print 'Reference is {0} (Westmere)'.format(ref)
print '{0:64}      {1:10s}  {2:10s}'.format('CPUINFERNO','E5-2680'   ,'X5570'  )
print '{0:64}      {1:10s}  {2:10s}'.format('TEST'      ,'SndyBrdg','Nehalem')
print '{0:64}      {1:10s}  {2:10s}'.format('CASE'      ,'vs.ref'  ,'vs.ref' )  
for k in order:

    ratio0=float(sandy[k])/float(westmere[k])
    ratio1=float(nehalem[k])/float(westmere[k])

    print '{0:64} {1:+10.1f}% {2:+10.1f}%'.format(
        k,
        100.0*ratio0-100.0,
        100.0*ratio1-100.0
        )
