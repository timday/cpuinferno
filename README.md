CPU Inferno
===========

Superseeded by threadinferno.

Anthology of benchmarks emphasizing memory bandwidth, heap & TLB manipulations
(and multithreading), for the purposes of probing potential and suspected issues
occurring with virtualization.

Platforms tested
================

* namche: Quad-core i7 (with HT), Debian/Wheezy amd64

* namche-vm-debian1: namche with Debian/Wheezy amd64 in Virtualbox (8 core)

* AWS EC2 c3.2xlarge: Intel Xeon E5-2680 @ 2.8GHz, 8 vCPU, 15GB. 
  Test with HVM and PVM.  And HVM with hugepages.

* AWS EC2 c1.xlarge: Intel Xeon E5410 @ 2.33GHz, 8 vCPU, 7GB
  Seems to be PVM only.
  A second time, got a (PVM) Intel Xeon E5506 @ 2.13GHz

EC2 AMIs: ubuntu: <http://cloud-images.ubuntu.com/locator/ec2/> has:

> us-east-1	saucy	Devel	amd64	hvm	20131204	ami-5ff8da36	hvm

> us-east-1	saucy	Devel	amd64	ebs	20131204	ami-5df8da34	aki-88aa75e1

Other possibilities
-------------------
* Try to get a CPU generation between c1 and c3.  m2.4xlarge was a E5-2665 0 @ 2.40GHz (Sandy Bridge).

* cc1.4xlarge maybe an interesting Xeon?  16 threads though.  Got a X5570 @ 2.93GHz (Nehalem).

* hi1.4xlarge reported as being Westmere here <http://techblog.netflix.com/2012/07/benchmarking-high-performance-io-with.html>.  Is a E5620 @ 2.40GHz

* Debian on EC2: <https://wiki.debian.org/Cloud/AmazonEC2Image/Wheezy> ; would have used in preference to Ubuntu but has only PVM AMIs.

* Google compute engine.  Appears to be KVM based.

* http://www.stormondemand.com/servers/ - Mentioned at <http://blog.cloudharmony.com/2010/09/benchmarking-of-ec2s-new-cluster.html> as having Westmere servers.

Setup
=====

Ubuntu
------
* `sudo aptitude install emacs mercurial make g++ libboost-dev libboost-test-dev libtbb-dev`
* Also need gcc-multilib for libhugetlbfs build.
* `mkdir project;cd project ; hg clone https://timday@bitbucket.org/timday/cpuinferno ; cd cpuinferno`
* `make && ./cpuinferno`

HugePages
---------
*CAUTION* TBB doesn't seem to play too nicely with libhugetlbfs' HUGETLB_MORECORE; blows up an extra few GByte.
Huge pages not really the main interest for now though.
Setting `vm.nr_hugepages = 3072` (6GB) seems to be good compromise between sufficient/insufficient on namche.

On AWS (with "admin" user; Ubuntu will use ubuntu): 

    groupadd hugetlbfs
    getent group hugetlbfs
    adduser admin hugetlbfs
    cat <<EOF >> /etc/sysctl.conf
    vm.nr_hugepages = 3072
    vm.hugetlb_shm_group = 1001
    EOF
    mkdir /hugepages
    cat <<EOF >> /etc/fstab
    hugetlbfs /hugepages hugetlbfs mode=1770,gid=1001 0 0
    EOF
    sync ; echo 3 > /proc/sys/vm/drop_caches
    sysctl -p
    cat /proc/meminfo
    mount /hugepages

libhugetlbfs: Run hugetlbfs/'s SETUP to install.

Run executable with `LD_PRELOAD=libhugetlbfs.so LD_LIBRARY_PATH=~/project/cpuinferno/hugetlbfs/libhugetlbfs-2.17/obj64 HUGETLB_MORECORE=yes`

Analysis
========

* results/analyse.txt: i7 920 reference (quad core, 8 thread, Skulltrail mobo), various 8 vthread EC2s, Virtualbox on reference.

* results/analyse-ec2.txt: Compares new c3 E5-2680s vs. old E5506 c1.  Quad core/8 thread.

* results/analyse-ec2-archs.txt: 3 Generations of EC2 virtualised HW <http://ark.intel.com/compare/47925,37111,64583>
    * ec2-c3.4xlarge-hvm-E5-2680 (Sandy Bridge) - 55 ECU.
    * ec2-hi1.4xlarge-hvm-E5620 (Westmere) - 35 ECU.
    * ec2-cc1.4xlarge-hvm-X5570 (Nehalem) -  33.5 ECU.
  Westmere performance seems odd.  

Other factors
=============

OpenMP
------

* OpenMP is quite configurable by environment variables. 
    * Intel: <http://software.intel.com/sites/products/documentation/doclib/iss/2013/compiler/cpp-lin/GUID-E1EC94AE-A13D-463E-B3C3-6D7A7205F5A1.htm>
    * Gnu OMP: <http://gcc.gnu.org/onlinedocs/libgomp.pdf‎>
    * OMP_WAIT_POLICY looks interesting ("active" or "passive"; Gnu also has intermediate behaviour on undefined)

Linux perf
----------

* Perf examples:
    * `sudo perf record ./cpuinferno --run_test=tbb_control_performance/tbb_threads_flipflop`
    * `OMP_WAIT_POLICY=active sudo -E perf record ./cpuinferno --run_test=mixed_tbb_omp/omp_interleaved_tbb_short`
    * `sudo perf report`
