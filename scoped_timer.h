#ifndef _scoped_timer_h_
#define _scoped_timer_h_

#include <boost/format.hpp>
#include <boost/noncopyable.hpp>

#include <iostream>

#include <tbb/tick_count.h>

// Utility class for timing and logging rates ("things-per-second").
// NB _any_ destructor invokation (including early return from a function
// or exception throw) will trigger a logging output which will assume
// that whatever is being measured has completed successfully and fully.
class scoped_timer : boost::noncopyable
{
public:

  scoped_timer(const std::string& what,const std::string& units,double n)
    :_what(what)
    ,_units(units)
    ,_how_many(n)
    ,_start(tbb::tick_count::now())
    {}

  ~scoped_timer()
  {
    tbb::tick_count stop=tbb::tick_count::now();
	const double t=(stop-_start).seconds();

    std::cout << (
      boost::format(
	"$ %1%: %|80t|%|2$-6.3g| %|3$|/s (%|4$-5.3g|s)"
      ) % _what % (_how_many/t) % _units % t
    ) << std::endl;
  }

private:

  const std::string _what;
  const std::string _units;
  const double _how_many;
  const tbb::tick_count _start;
};

#endif
