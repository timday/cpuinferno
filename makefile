
cpuinferno:	cpuinferno.cpp cpuinferno_functions.h cpuinferno_functions.cpp force.h force.cpp scoped_timer.h makefile
		g++ -std=c++11 -o cpuinferno -march=native -mno-avx -O3 -g -fopenmp -fstrict-aliasing cpuinferno.cpp cpuinferno_functions.cpp force.cpp -lboost_unit_test_framework -ltbb -ltbbmalloc
