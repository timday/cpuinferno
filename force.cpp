#include "force.h"

volatile bool dump;

void force(char v) {dump=(v!=0);}

void force(int v) {dump=(v!=0);}

void force(size_t v) {dump=(v!=0);}

void force(int64_t v) {dump=(v!=0);}

void force(double v) {dump=(v!=0.0);}
