#ifndef WIN32  // Use static linking on win32
#define BOOST_TEST_DYN_LINK
#endif

#ifdef WIN32
#pragma  warning( disable : 4180 )  // Disable "qualifier applied to function type has no meaning; ignored" from a const by-value return in TBB code (parallel_invoke).
#endif

#define BOOST_TEST_MODULE cpuinferno
#include <boost/test/unit_test.hpp>

#include <algorithm>
#include <boost/lexical_cast.hpp>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <emmintrin.h>
#include <functional>
#include <iostream>
#include <omp.h>
#include <random>
#include <tbb/scalable_allocator.h>
#include <tbb/parallel_for.h>
#include <tbb/parallel_invoke.h>
#include <tbb/parallel_reduce.h>
#include <tbb/task_scheduler_init.h>
#include <vector>

#ifndef WIN32
#include <sys/mman.h>
#endif

#define TBB_PREVIEW_MEMORY_POOL 1
#include <tbb/memory_pool.h>

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <windows.h>
void sleep(int s) {Sleep(1000*s);}
#endif

#include "cpuinferno_functions.h"
#include "force.h"
#include "scoped_timer.h"

const size_t gbyte=(1<<30);

const float kilo=(1<<10);
const float mega=(1<<20);
const float giga=(1<<30);

const size_t N=4;  // Will use about this many GB (maximum) for buffers.

tbb::task_scheduler_init tbb_sched;

struct GlobalFixture;
const GlobalFixture* config;

struct GlobalFixture {
  GlobalFixture()
    :maxthreads(std::min(tbb::task_scheduler_init::default_num_threads(),omp_get_max_threads()))
  {
    omp_set_dynamic(0);

    std::cout << "TBB default threads is   " << tbb::task_scheduler_init::default_num_threads() << std::endl;
    std::cout << "OpenMP max threads is    " << omp_get_max_threads() << std::endl;
    
    const char*const threadenv=getenv("CPUINFERNO_MAXTHREADS");
    if (threadenv!=0) {
      maxthreads=boost::lexical_cast<int>(threadenv);
      if (maxthreads==0) throw std::runtime_error("Bad CPUINFERNO_MAXTHREADS value");
    }
    
    std::cout << "Configured maxthreads is " << maxthreads << std::endl;
    
    config=this;
  }
  
  ~GlobalFixture() {
    tbb_sched.terminate();
  }
  int maxthreads;
};

BOOST_GLOBAL_FIXTURE(GlobalFixture);

void set_tbb_threads(int threads) {
  tbb_sched.terminate();
  tbb_sched.initialize(threads);
}

BOOST_AUTO_TEST_CASE(sanity_check) {
  BOOST_REQUIRE_EQUAL(sizeof(size_t),8);  // 32bit is so last century; just give up now.
  BOOST_REQUIRE_EQUAL(sizeof(__m128i),16);
  BOOST_REQUIRE_EQUAL(sizeof(int64_t),8);

#ifdef WIN32
#ifndef _WIN64
#pragma message("Expecting 64bit build!")
#endif
#endif
}

BOOST_AUTO_TEST_CASE(assumptions) {
  BOOST_CHECK_LE(config->maxthreads,tbb::task_scheduler_init::default_num_threads());
  BOOST_CHECK_LE(config->maxthreads,omp_get_max_threads());
}

// Just to prime memory consumption "high water mark"
BOOST_AUTO_TEST_CASE(warmstart) {
  std::vector<char> dst(N*gbyte,0);
  memset(&dst[0],0,dst.size());
}

BOOST_AUTO_TEST_SUITE(cpu_performance)

void hailstone_test(const size_t reps,const size_t n,const std::string& qualifier) {
  scoped_timer timer("hailstone numbers "+qualifier,"GigaIterations",reps*n/giga);
  size_t t=0;
  for (size_t s=0;s<reps;++s) {
    t+=iterate_hailstone(s,n);
  }
  force(t);
}

BOOST_AUTO_TEST_CASE(hailstone) {
  hailstone_test(256,(1<<20),"(few long)");
  hailstone_test((1<<20),256,"(many short)");
}

void tbb_hailstone_test(const size_t reps,const size_t n,const std::string& qualifier) {
  scoped_timer timer(boost::str(boost::format("tbb parallel_reduce hailstone numbers %1%") % qualifier),"GigaIterations",reps*n/giga);
  const size_t t=tbb::parallel_reduce(
    tbb::blocked_range<size_t>(0,reps),
    size_t(0),
    [n](const tbb::blocked_range<size_t>& r,size_t v) -> size_t {
      for (size_t s=r.begin();s<r.end();++s) {
        v+=iterate_hailstone(s,n);
      }
      return v;
    },
    [](size_t a,size_t b) -> size_t {return a+b;}
  );
  force(t);
}

BOOST_AUTO_TEST_CASE(tbb_hailstone) {
  for (int threads=1;threads<=config->maxthreads;++threads) {
    set_tbb_threads(threads); 
    tbb_hailstone_test(256,(1<<20),boost::str(boost::format("(few long) (x%1% threads)") % threads));
    tbb_hailstone_test((1<<20),256,boost::str(boost::format("(many short) (x%1% threads)") % threads));
  }
}

// MS OpenMP loop control must be signed int type, so uses int64_t instead of size_t
void omp_hailstone_test(int64_t reps,const size_t n,const int threads,const std::string& qualifier) {
  scoped_timer timer(boost::str(boost::format("omp parallel_reduce hailstone numbers %1%") % qualifier),"GigaIterations",reps*n/giga);
  size_t v=0;
#pragma omp parallel for reduction (+:v) num_threads(threads)
  for (int64_t s=0;s<reps;++s)
    v+=iterate_hailstone(s,n);
  force(v);
}

BOOST_AUTO_TEST_CASE(omp_hailstone) {
  for (int threads=1;threads<=config->maxthreads;++threads) {
    omp_hailstone_test(256,(1<<20),threads,boost::str(boost::format("(few long) (x%1% threads)") % threads));
    omp_hailstone_test((1<<20),256,threads,boost::str(boost::format("(many short) (x%1% threads)") % threads));
  }
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(tbb_control_performance)

BOOST_AUTO_TEST_CASE(tbb_threads_rampup) {
  const size_t reps=10000;
  
  scoped_timer timer("tbb threads rampup","K-terminate&reinit calls",reps/kilo);
  for (size_t i=0;i<reps;++i) {
    set_tbb_threads(1+i%config->maxthreads);
  }
}

BOOST_AUTO_TEST_CASE(tbb_threads_rampdown) {
  const size_t reps=10000;
  
  scoped_timer timer("tbb threads rampdown","K-terminate&reinit calls",reps/kilo);
  for (size_t i=0;i<reps;++i) {
    set_tbb_threads(1+(reps-i)%config->maxthreads);
  }
}

BOOST_AUTO_TEST_CASE(tbb_threads_flipflop) {
  const size_t reps=10000;
  
  scoped_timer timer("tbb threads flipflop","K-terminate&reinit calls",reps/kilo);
  for (size_t i=0;i<reps;++i) {
    set_tbb_threads(i&1 ? 1 : config->maxthreads);
  }
}

BOOST_AUTO_TEST_CASE(tbb_threads_flipflop_lo) {
  const size_t reps=10000;
  
  scoped_timer timer("tbb threads flipflop lo","K-terminate&reinit calls",reps/kilo);
  for (size_t i=0;i<reps;++i) {
    set_tbb_threads(i&1 ? 1 : 2);
  }
}

BOOST_AUTO_TEST_CASE(tbb_threads_flipflop_hi) {
  const size_t reps=10000;
  
  scoped_timer timer("tbb threads flipflop hi","K-terminate&reinit calls",reps/kilo);
  for (size_t i=0;i<reps;++i) {
    set_tbb_threads(i&1 ? config->maxthreads-1 : config->maxthreads);
  }
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(omp_control_performance)

const size_t reps=(1<<24);

BOOST_AUTO_TEST_CASE(omp_threads_rampup) {
  scoped_timer timer("omp threads rampup","Mega-calls",reps/mega);
  for (size_t i=0;i<reps;++i) {
    omp_set_num_threads(1+i%config->maxthreads);
  }
}

BOOST_AUTO_TEST_CASE(omp_threads_rampdown) {
  scoped_timer timer("omp threads rampdown","Mega-calls",reps/mega);
  for (size_t i=0;i<reps;++i) {
    omp_set_num_threads(1+(reps-i)%config->maxthreads);
  }
}

BOOST_AUTO_TEST_CASE(omp_threads_flipflop) {
  scoped_timer timer("omp threads flipflop","Mega-calls",reps/mega);
  for (size_t i=0;i<reps;++i) {
    omp_set_num_threads(i&1 ? 1 : config->maxthreads);
  }
}

BOOST_AUTO_TEST_CASE(omp_threads_flipflop_lo) {
  scoped_timer timer("omp threads flipflop lo","Mega-calls",reps/mega);
  for (size_t i=0;i<reps;++i) {
    omp_set_num_threads(i&1 ? 1 : 2);
  }
}

BOOST_AUTO_TEST_CASE(omp_threads_flipflop_hi) {
  scoped_timer timer("omp threads flipflop hi","Mega-calls",reps/mega);
  for (size_t i=0;i<reps;++i) {
    omp_set_num_threads(i&1 ? config->maxthreads-1 : config->maxthreads);
  }
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(tbb_control_and_use_performance)

BOOST_AUTO_TEST_CASE(tbb_threads_used_flipflop) {
  const size_t reps=1000;
  const int n=(1<<20);

  {
    scoped_timer timer("tbb threads used slow flipflop","K-iterations",reps/kilo);
    set_tbb_threads(1);
    for (size_t i=0;i<reps;++i) {
      if (i==reps/2) set_tbb_threads(config->maxthreads);
      const int t=tbb::parallel_reduce(
        tbb::blocked_range<int>(0,n),
        0,
        [](const tbb::blocked_range<int>& r,int v) -> int {
          for (int i=r.begin();i<r.end();++i) v+=i;
          return v;
        },
        [](int x,int y) {return x+y;}
      );
      force(t);
    }
  }

  {  
    scoped_timer timer("tbb threads used fast flipflop","K-iterations",reps/kilo);
    for (size_t i=0;i<reps;++i) {
      set_tbb_threads(i&1 ? 1 : config->maxthreads);
      const int t=tbb::parallel_reduce(
        tbb::blocked_range<int>(0,n),
        0,
        [](const tbb::blocked_range<int>& r,int v) -> int {
          for (int i=r.begin();i<r.end();++i) v+=i;
          return v;
        },
        [](int x,int y) {return x+y;}
      );
      force(t);
    }
  }
}
  
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(tbb_control_and_use_performance)

BOOST_AUTO_TEST_CASE(omp_threads_used_flipflop) {
  const size_t reps=1000;
  const int n=(1<<20);

  {  
    scoped_timer timer("omp threads used slow flipflop","K-iterations",reps/kilo);
    omp_set_num_threads(1);
    for (size_t i=0;i<reps;++i) {
      if (i==reps/2) omp_set_num_threads(config->maxthreads);
      int t=0;
#pragma omp parallel for reduction (+:t)
      for (int v=0;v<n;++v) {
	      t+=v;
      }
      force(t);
    }
  }

  {  
    scoped_timer timer("omp threads used fast flipflop","K-iterations",reps/kilo);
    for (size_t i=0;i<reps;++i) {
      omp_set_num_threads(i&1 ? 1 : config->maxthreads);
      int t=0;
#pragma omp parallel for reduction (+:t)
      for (int v=0;v<n;++v) {
	      t+=v;
      }
      force(t);
    }
  }

  {  
    scoped_timer timer("omp threads used fast flipflop pragma","K-iterations",reps/kilo);
    for (size_t i=0;i<reps;++i) {
      int t=0;
#pragma omp parallel for reduction (+:t) num_threads(i&1 ? 1 : config->maxthreads)
      for (int v=0;v<n;++v) {
	      t+=v;
      }
      force(t);
    }
  }

  
  set_tbb_threads(config->maxthreads);
  omp_set_num_threads(config->maxthreads);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(mixed_tbb_omp)

void omp_job(const int n) {
  int t=0;
#pragma omp parallel for reduction (+:t)
  for (int v=0;v<n;++v) {
    t+=v;
  }
  force(t);
}

void tbb_job(const int n) {
  const size_t t=tbb::parallel_reduce(
    tbb::blocked_range<int>(0,n),
    0,
    [](const tbb::blocked_range<int>& r,int v) -> int {
      for (int i=r.begin();i<r.end();++i) v+=i;
      return v;
    },
    [](int x,int y) {return x+y;}
  );
  force(t);
}

const size_t reps=(1<<6);
const int nlo=(1<<24);
const int nvlo=nlo/reps;
const int nhi=nlo*reps;;

void omp_job_long() {omp_job(nhi);}
void omp_job_short() {omp_job(nlo);}
void omp_job_vshort() {omp_job(nvlo);}
void tbb_job_long() {tbb_job(nhi);}
void tbb_job_short() {tbb_job(nlo);}
void tbb_job_vshort() {tbb_job(nvlo);}

BOOST_AUTO_TEST_CASE(tbb_then_omp) {
  sleep(1);
  {
    scoped_timer timer("tbb then omp","Long jobs",2);
    tbb_job_long();
    omp_job_long();
  }
}

BOOST_AUTO_TEST_CASE(omp_then_tbb) {
  sleep(1);
  {
    scoped_timer timer("omp then tbb","Long jobs",2);
    omp_job_long();
    tbb_job_long();
  }
}

BOOST_AUTO_TEST_CASE(tbb_and_omp) {
  sleep(1);
  {
    scoped_timer timer("tbb and omp","Long jobs",2);
    tbb::parallel_invoke(tbb_job_long,omp_job_long);
  }
}

BOOST_AUTO_TEST_CASE(tbb_and_tbb) {
  sleep(1);
  {
    scoped_timer timer("tbb and tbb","Long jobs",2);
    tbb::parallel_invoke(tbb_job_long,tbb_job_long);
  }
}

BOOST_AUTO_TEST_CASE(omp_and_omp) {
  sleep(1);
  {
    scoped_timer timer("omp and omp","Long jobs",2);
    tbb::parallel_invoke(omp_job_long,omp_job_long);
  }
}

BOOST_AUTO_TEST_CASE(tbb_then_omp_short) {
  sleep(1);
  {
    scoped_timer timer("tbb then omp (short)","Short jobs",2*reps);
    for (size_t i=0;i<reps;++i) {
      tbb_job_short();
    }
    for (size_t i=0;i<reps;++i) {
      omp_job_short();
    }
  }
}

BOOST_AUTO_TEST_CASE(omp_then_tbb_short) {
  sleep(1);
  {
    scoped_timer timer("omp then tbb (short)","Short jobs",2*reps);
    for (size_t i=0;i<reps;++i) {
      omp_job_short();
    }
    for (size_t i=0;i<reps;++i) {
      tbb_job_short();
    }
  }
}

BOOST_AUTO_TEST_CASE(omp_interleaved_tbb_short) {
  sleep(1);
  {
    scoped_timer timer("omp interleaved tbb (short)","Short jobs",2*reps);
    for (size_t i=0;i<reps;++i) {
      omp_job_short();
      tbb_job_short();
    }
  }
}

BOOST_AUTO_TEST_CASE(tbb_then_omp_vshort) {
  sleep(1);
  {
    scoped_timer timer("tbb then omp (very short)","Kilo very short jobs",2*reps*reps/kilo);
    for (size_t i=0;i<reps*reps;++i) {
      tbb_job_vshort();
    }
    for (size_t i=0;i<reps*reps;++i) {
      omp_job_vshort();
    }
  }
}

BOOST_AUTO_TEST_CASE(omp_then_tbb_vshort) {
  sleep(1);
  {
    scoped_timer timer("omp then tbb (very short)","Kilo very short jobs",2*reps*reps/kilo);
    for (size_t i=0;i<reps*reps;++i) {
      omp_job_vshort();
    }
    for (size_t i=0;i<reps*reps;++i) {
      tbb_job_vshort();
    }
  }
}

BOOST_AUTO_TEST_CASE(omp_interleaved_tbb_vshort) {
  sleep(1);
  {
    scoped_timer timer("omp interleaved tbb (very short)","Kilo very short jobs",2*reps*reps/kilo);
    for (size_t i=0;i<reps*reps;++i) {
      omp_job_vshort();
      tbb_job_vshort();
    }
  }
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(memory_performance)

// Prime "high water mark" again
BOOST_AUTO_TEST_CASE(warmstart) {
  std::vector<char> dst(N*gbyte,0);
  memset(&dst[0],0,dst.size());
}

BOOST_AUTO_TEST_CASE(memset_performance) {
  std::vector<char> dst(N*gbyte,0);
  {
    scoped_timer timer("memset","GByte",dst.size()/giga);
    memset(&dst[0],0,dst.size());
  } 
}

BOOST_AUTO_TEST_CASE(memsum_performance) {
  std::vector<char> src(N*gbyte,1);
  {
    scoped_timer timer("memsum","GByte",src.size()/giga);
    const char t=memsum(&src[0],src.size());
    force(t);
  } 
}

BOOST_AUTO_TEST_CASE(memcpy_performance) {

  std::vector<char> src(N*gbyte/2,1);
  std::vector<char> dst(src.size(),0);

  {
    scoped_timer timer("memcpy","GByte",src.size()/giga);
    memcpy(&dst[0],&src[0],dst.size());
  }
}

BOOST_AUTO_TEST_CASE(tbb_memset_performance) {

  std::vector<char> dst(N*gbyte,0);

  for (int threads=1;threads<=config->maxthreads;++threads) {
    set_tbb_threads(threads);    
    {
      scoped_timer timer(boost::str(boost::format("tbb parallel_for memset (x%1% threads)") % threads),"GByte",dst.size()/giga);
      tbb::parallel_for(
        tbb::blocked_range<size_t>(0,dst.size()),
        [&dst](const tbb::blocked_range<size_t>& r) {
          memset(&dst[r.begin()],0,r.size());
        }
      );
    }
  }
}

BOOST_AUTO_TEST_CASE(tbb_memsum_performance) {

  std::vector<char> src(N*gbyte,1);

  for (int threads=1;threads<=config->maxthreads;++threads) {
    set_tbb_threads(threads);
    {
      scoped_timer timer(boost::str(boost::format("tbb parallel_reduce memsum (x%1% threads)") % threads),"GByte",src.size()/giga);
      const size_t t=tbb::parallel_reduce(
        tbb::blocked_range<size_t>(0,src.size()),
        char(0),
        [&src](const tbb::blocked_range<size_t>& r,char v) -> char {
          return v+memsum(&src[r.begin()],r.size());
        },
        [](char x,char y) -> char{return x+y;}
      );
      force(t);
    }
  }
}

BOOST_AUTO_TEST_CASE(tbb_memcpy_performance) {

  std::vector<char> src(N*gbyte/2,1);
  std::vector<char> dst(src.size(),0);

  for (int threads=1;threads<=config->maxthreads;++threads) {
    set_tbb_threads(threads);    
    {
      scoped_timer timer(boost::str(boost::format("tbb parallel_for memcpy (x%1% threads)") % threads),"GByte",src.size()/giga);
      tbb::parallel_for(
        tbb::blocked_range<size_t>(0,dst.size()),
        [&src,&dst](const tbb::blocked_range<size_t>& r) {
          memcpy(&dst[r.begin()],&src[r.begin()],r.size());
        }
      );
    }
  }
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(tlb_performance)

BOOST_AUTO_TEST_CASE(page_strided_access_performance) {

  const size_t stride=4096;
  const size_t reps=10;
  std::vector<char> src(N*gbyte,1);
  std::vector<size_t> idx;
  for (size_t i=0;i<src.size();i+=stride) idx.push_back(i);

  {
    scoped_timer t("page strided write sequential","MegaAccess",reps*idx.size()/mega);
    for (size_t i=0;i<reps;++i) {
      for (size_t i=0;i<idx.size();++i) {
        src[idx[i]]=2;
      }
    }
  }  

  {
    scoped_timer t("page strided read sequential","MegaAccess",reps*idx.size()/mega);
    char hash=0;
    for (size_t i=0;i<reps;++i) {
      for (size_t i=0;i<idx.size();++i) {
        hash=(hash^src[idx[i]]);
      }
    }
    force(hash);
  }

  std::mt19937 rng(23);
  std::function<size_t(int)> r0n=[&rng](int n) -> int {std::uniform_int_distribution<int> dist(0,n);return dist(rng);};
  
  std::random_shuffle(idx.begin(),idx.end(),r0n);
  
  {
    scoped_timer t("page strided write random","MegaAccess",reps*idx.size()/mega);
    for (size_t i=0;i<reps;++i) {
      for (size_t i=0;i<idx.size();++i) {
        src[idx[i]]=2;
      }
    }
  }

  {
    scoped_timer t("page strided read random","MegaAccess",reps*idx.size()/mega);
    char hash=0;
    for (size_t i=0;i<reps;++i) {
      for (size_t i=0;i<idx.size();++i) {
        hash=(hash^src[idx[i]]);
      }
    }
    force(hash);
  }
}

BOOST_AUTO_TEST_CASE(tbb_page_strided_access_performance) {

  const size_t stride=4096;
  const size_t reps=10;
  std::vector<char> src(N*gbyte,1);
  std::vector<size_t> idx;
  for (size_t i=0;i<src.size();i+=stride) idx.push_back(i);

  for (int threads=1;threads<=config->maxthreads;++threads) {
    set_tbb_threads(threads);

    {
      scoped_timer t(boost::str(boost::format("tbb parallel_for page strided write sequential (x%1% threads)") % threads),"MegaAccess",reps*idx.size()/mega);
      for (size_t i=0;i<reps;++i) {
        tbb::parallel_for(
          tbb::blocked_range<size_t>(0,idx.size()),
          [&src,&idx](const tbb::blocked_range<size_t>& r) {for (size_t i=r.begin();i<r.end();++i) src[idx[i]]=2;}
        );
      }
    }  

    {
      scoped_timer t(boost::str(boost::format("tbb parallel_reduce page strided read sequential (x%1% threads)") % threads),"MegaAccess",reps*idx.size()/mega);
      char hash=0;
      for (size_t i=0;i<reps;++i) {
        tbb::parallel_reduce(
          tbb::blocked_range<size_t>(0,idx.size()),
          0,
          [&src,&idx](const tbb::blocked_range<size_t>& r,char v) -> char {for (size_t i=r.begin();i<r.end();++i) v=(v^src[idx[i]]);return v;},
          [](char x,char y) {return x^y;}
        );
      }
      force(hash);
    }
  }

  {
    std::mt19937 rng(23);
    std::function<size_t(size_t)> r0n=[&rng](size_t n) -> size_t {std::uniform_int_distribution<size_t> dist(0,n);return dist(rng);};
    std::random_shuffle(idx.begin(),idx.end(),r0n);
  }
  
  for (int threads=1;threads<=config->maxthreads;++threads) {
    set_tbb_threads(threads);

    {
      scoped_timer t(boost::str(boost::format("tbb parallel_for page strided write random (x%1% threads)") % threads),"MegaAccess",reps*idx.size()/mega);
      for (size_t i=0;i<reps;++i) {
        tbb::parallel_for(
          tbb::blocked_range<size_t>(0,idx.size()),
          [&src,&idx](const tbb::blocked_range<size_t>& r) {for (size_t i=r.begin();i<r.end();++i) src[idx[i]]=2;}
        );
      }
    }

    {
      scoped_timer t(boost::str(boost::format("tbb parallel_reduce page strided read random (x%1% threads)") % threads),"MegaAccess",reps*idx.size()/mega);
      char hash=0;
      for (size_t i=0;i<reps;++i) {
        tbb::parallel_reduce(
          tbb::blocked_range<size_t>(0,idx.size()),
          0,
          [&src,&idx](const tbb::blocked_range<size_t>& r,char v) -> char {for (size_t i=r.begin();i<r.end();++i) v=(v^src[idx[i]]);return v;},
          [](char x,char y) {return x^y;}
        );
      }
      force(hash);
    }
  }
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(heap_performance) // NB Had better be last or scalable allocator stuff will stay there

struct probe4k {
  char _payload[4096];
};

const size_t n1=N*((1<<30)/2/64);
const size_t n4k=N*((1<<30)/2/4096);

template <typename T> void test_malloc_free_performance(const size_t n,const std::string& txt) {
  std::vector<T*> items(n,0);

  {
    scoped_timer timer(boost::str(boost::format("malloc %1%") % txt),"MegaAllocations",n/mega);
    for (size_t i=0;i<items.capacity();++i) {
      items[i]=reinterpret_cast<T*>(malloc(sizeof(T)));
    }
  }

  {
    scoped_timer timer(boost::str(boost::format("free FIFO %1%") % txt),"MegaDeallocations",n/mega);
    for (size_t i=0;i<items.capacity();++i) {
      free(items[i]);
    }
  }

  {
    scoped_timer timer(boost::str(boost::format("malloc again %1%") % txt),"MegaAllocations",n/mega);
    for (size_t i=0;i<items.capacity();++i) {
      items[i]=reinterpret_cast<T*>(malloc(sizeof(T)));
    }
  }

  {
    scoped_timer timer(boost::str(boost::format("free LIFO %1%") % txt),"MegaDeallocations",n/mega);
    for (size_t i=0;i<items.capacity();++i) {
      free(items[items.capacity()-1-i]);
    }
  }
}

BOOST_AUTO_TEST_CASE(malloc_free_performance) {
  test_malloc_free_performance<char>(n1,"1");
  test_malloc_free_performance<probe4k>(n4k,"4k");
}

template <typename T> void test_std_allocator_performance(const size_t n,const std::string& txt) {
  std::vector<T*> items(n,0);
  std::allocator<T> allocator;

  {
    scoped_timer timer(boost::str(boost::format("std allocate %1%") % txt),"MegaAllocations",n/mega);
    for (size_t i=0;i<items.capacity();++i) {
      items[i]=allocator.allocate(1);
    }
  }

  {
    scoped_timer timer(boost::str(boost::format("std deallocate FIFO %1%") % txt),"MegaDeallocations",n/mega);
    for (size_t i=0;i<items.capacity();++i) {
      allocator.deallocate(items[i],1);
    }
  }

  {
    scoped_timer timer(boost::str(boost::format("std allocate again %1%") % txt),"MegaAllocations",n/mega);
    for (size_t i=0;i<items.capacity();++i) {
      items[i]=allocator.allocate(1);
    }
  }

  {
    scoped_timer timer(boost::str(boost::format("std deallocate LIFO %1%") % txt),"MegaDeallocations",n/mega);
    for (size_t i=0;i<items.capacity();++i) {
      allocator.deallocate(items[items.capacity()-1-i],1);
    }
  }
}

BOOST_AUTO_TEST_CASE(std_allocator_performance) {

  test_std_allocator_performance<char>(n1,"1");
  test_std_allocator_performance<probe4k>(n4k,"4k");
}

template <typename T> void test_tbb_std_allocator_performance(const size_t n,const std::string& txt) {

  for (int threads=1;threads<=config->maxthreads;++threads) {
    set_tbb_threads(threads);
    std::vector<T*> items(n,0);
    std::allocator<T> allocator;

    {
      scoped_timer timer(boost::str(boost::format("tbb parallel_for std allocate %1% (x%2% threads)") % txt % threads),"MegaAllocations",n/mega);
      tbb::parallel_for(
        tbb::blocked_range<size_t>(0,n),
        [&items,&allocator](const tbb::blocked_range<size_t>& r) {
          for (size_t i=r.begin();i<r.end();++i) {
            items[i]=allocator.allocate(1);
          }
        }
      );
    }

    {
      scoped_timer timer(boost::str(boost::format("tbb parallel_for std deallocate %1% (x%2% threads)") % txt % threads),"MegaDeallocations",n/mega);
      tbb::parallel_for(
        tbb::blocked_range<size_t>(0,n),
        [&items,&allocator](const tbb::blocked_range<size_t>& r) {
          for (size_t i=r.begin();i<r.end();++i) {
            allocator.deallocate(items[i],1);
          }
        }
      );
    }
  }
}

BOOST_AUTO_TEST_CASE(tbb_std_allocator_performance) {
  test_tbb_std_allocator_performance<char>(n1,"1");
  test_tbb_std_allocator_performance<probe4k>(n4k,"4k");
}

template <typename T> void test_scalable_pool_allocator_performance(const size_t n,const std::string& txt) {
  std::vector<T*> items(n,0);
  tbb::memory_pool<std::allocator<char>> pool;
  tbb::memory_pool_allocator<T> allocator(pool);
  
  {
    scoped_timer timer(boost::str(boost::format("tbb scalable pool allocate %1%") % txt),"MegaAllocations",n/mega);
    for (size_t i=0;i<items.capacity();++i) {
      items[i]=allocator.allocate(1);
    }
  }
  
  {
    scoped_timer timer(boost::str(boost::format("tbb scalable pool deallocate %1%") % txt),"MegaDeallocations",n/mega);
    for (size_t i=0;i<items.capacity();++i) {
      allocator.deallocate(items[i],1);
    }
  }
}

BOOST_AUTO_TEST_CASE(scalable_pool_allocator_performance) {
  test_scalable_pool_allocator_performance<char>(n1,"1");
  test_scalable_pool_allocator_performance<probe4k>(n4k,"4k");
}

template <typename T> void test_tbb_scalable_pool_allocator_performance(const size_t n,const std::string& txt) {
  for (int threads=1;threads<=config->maxthreads;++threads) { 
    set_tbb_threads(threads);
    std::vector<T*> items(n,0);
    tbb::memory_pool<std::allocator<char>> pool;
    tbb::memory_pool_allocator<T> allocator(pool);

    {
      scoped_timer timer(boost::str(boost::format("tbb parallel_for tbb scalable pool allocate %1% (x%2% threads)") % txt % threads),"MegaAllocations",n/mega);
      tbb::parallel_for(
        tbb::blocked_range<size_t>(0,n),
        [&items,&allocator](const tbb::blocked_range<size_t>& r) {
          for (size_t i=r.begin();i<r.end();++i) {
            items[i]=allocator.allocate(1);
          }
        }
      );
    }
  
    {
      scoped_timer timer(boost::str(boost::format("tbb parallel_for tbb scalable pool deallocate %1% (x%2% threads)") % txt % threads),"MegaDeallocations",n/mega);
      tbb::parallel_for(
        tbb::blocked_range<size_t>(0,n),
        [&items,&allocator](const tbb::blocked_range<size_t>& r) {
          for (size_t i=r.begin();i<r.end();++i) {
            allocator.deallocate(items[i],1);
          }
        }
      );
    }
  }
}

BOOST_AUTO_TEST_CASE(tbb_scalable_pool_allocator_performance) {
  test_tbb_scalable_pool_allocator_performance<char>(n1,"1");
  test_tbb_scalable_pool_allocator_performance<probe4k>(n4k,"4k");
}

template <typename T> void test_scalable_allocator_performance(const size_t n,const std::string& txt) {
  std::vector<T*> items(n,0);
  tbb::scalable_allocator<T> allocator;
  
  {
    scoped_timer timer(boost::str(boost::format("tbb scalable allocate %1%") % txt),"MegaAllocations",n/mega);
    for (size_t i=0;i<items.capacity();++i) {
      items[i]=allocator.allocate(1);
    }
  }
  
  {
    scoped_timer timer(boost::str(boost::format("tbb scalable deallocate %1%") % txt),"MegaDeallocations",n/mega);
    for (size_t i=0;i<items.capacity();++i) {
      allocator.deallocate(items[i],1);
    }
  }
}

BOOST_AUTO_TEST_CASE(scalable_allocator_performance) {
  test_scalable_allocator_performance<char>(n1,"1");
  test_scalable_allocator_performance<probe4k>(n4k,"4k");
}

template <typename T> void test_tbb_scalable_allocator_performance(const size_t n,const std::string& txt) {
  for (int threads=1;threads<=config->maxthreads;++threads) { 
    set_tbb_threads(threads);
    std::vector<T*> items(n,0);
    tbb::scalable_allocator<T> allocator;

    {
      scoped_timer timer(boost::str(boost::format("tbb parallel_for tbb scalable allocate %1% (x%2% threads)") % txt % threads),"MegaAllocations",n/mega);
      tbb::parallel_for(
        tbb::blocked_range<size_t>(0,n),
        [&items,&allocator](const tbb::blocked_range<size_t>& r) {
          for (size_t i=r.begin();i<r.end();++i) {
            items[i]=allocator.allocate(1);
          }
        }
      );
    }
  
    {
      scoped_timer timer(boost::str(boost::format("tbb parallel_for tbb scalable deallocate %1% (x%2% threads)") % txt % threads),"MegaDeallocations",n/mega);
      tbb::parallel_for(
        tbb::blocked_range<size_t>(0,n),
        [&items,&allocator](const tbb::blocked_range<size_t>& r) {
          for (size_t i=r.begin();i<r.end();++i) {
            allocator.deallocate(items[i],1);
          }
        }
      );
    }
  }
}

BOOST_AUTO_TEST_CASE(tbb_scalable_allocator_performance) {
  test_tbb_scalable_allocator_performance<char>(n1,"1");
  test_tbb_scalable_allocator_performance<probe4k>(n4k,"4k");  
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(mmap_performance)

const size_t pagesize4k=4096;
const size_t npages=(N<<30)/pagesize4k;

BOOST_AUTO_TEST_CASE(warmup) {
  
  std::vector<void*> pages(npages,0);
#ifdef WIN32
  for (size_t i=0;i<pages.size();++i) {
    pages[i]=VirtualAlloc(0,pagesize4k,MEM_RESERVE|MEM_COMMIT,PAGE_READWRITE);
    *reinterpret_cast<char*>(pages[i])=1;
  }
  BOOST_CHECK(std::find(pages.begin(),pages.end(),nullptr)==pages.end()); // Check all allocs succeeded
  bool trouble=false;
  for (size_t i=0;i<pages.size();++i) {
    const BOOL err=VirtualFree(pages[i],0,MEM_RELEASE);
    if (err==0) trouble=true;
  }
  BOOST_CHECK(!trouble);
#else
  for (size_t i=0;i<pages.size();++i) {
    pages[i]=mmap(0,pagesize4k,PROT_READ|PROT_WRITE,MAP_PRIVATE|MAP_ANONYMOUS,-1,0);
    *reinterpret_cast<char*>(pages[i])=1;
  }
  BOOST_CHECK(std::find(pages.begin(),pages.end(),nullptr)==pages.end()); // Check all allocs succeeded
  bool trouble=false;
  for (size_t i=0;i<pages.size();++i) {
    const int err=munmap(pages[i],pagesize4k);
    if (err!=0) trouble=true;
  }
  BOOST_CHECK(!trouble);
#endif
}

BOOST_AUTO_TEST_CASE(map_4k_pages_performance) {

  const size_t n=N*(1<<30)/pagesize4k;

  for (int precommit=0;precommit<=1;++precommit) {
    const std::string qualifier(precommit ? "(early commit)" : "(late commit)");
    std::vector<void*> pages(n,0);

    {
      scoped_timer timer(boost::str(boost::format("map 4k pages %1%") % qualifier),"MegaMaps",n/mega);
      for (size_t i=0;i<pages.size();++i) {
#ifdef WIN32
        pages[i]=VirtualAlloc(0,pagesize4k,MEM_RESERVE|(precommit ? MEM_COMMIT : 0),PAGE_READWRITE);
#else
        pages[i]=mmap(0,pagesize4k,PROT_READ|PROT_WRITE,MAP_PRIVATE|MAP_ANONYMOUS|(precommit?MAP_POPULATE:0),-1,0);
#endif
      }
    }

    BOOST_CHECK(std::find(pages.begin(),pages.end(),nullptr)==pages.end());

    {
      scoped_timer timer(boost::str(boost::format("touch pages %1%") % qualifier),"MegaTouches",n/mega);
      touch_4k_pages(pages,(precommit==1));
    }

    {
      scoped_timer timer(boost::str(boost::format("touch pages again %1%") % qualifier),"MegaTouches",n/mega);
      touch_4k_pages(pages,true);
    }

    bool trouble=false;
    {
      scoped_timer timer(boost::str(boost::format("unmap pages %1%") % qualifier),"MegaUnmaps",n/mega);
      for (size_t i=0;i<pages.size();++i) {
#ifdef WIN32
       const BOOL err=VirtualFree(pages[i],0,MEM_RELEASE);  // NB must use zero size with MEM_RELEASE
       if (err==0) trouble=true;
#else
       const int err=munmap(pages[i],pagesize4k);
       if (err!=0) trouble=true;
#endif
      }
    }
    BOOST_CHECK(!trouble);
  }
}

BOOST_AUTO_TEST_SUITE_END()
