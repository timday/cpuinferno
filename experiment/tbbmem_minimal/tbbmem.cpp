#include <array>
#include <boost/format.hpp>
#include <fstream>
#include <iostream>
#include <tbb/parallel_for.h>
#include <tbb/scalable_allocator.h>
#include <tbb/task_scheduler_init.h>

void log_process_size(const std::string& msg) {
  std::ifstream in("/proc/self/statm");
  size_t pages(0);
  in >> pages;
  if (!in) throw std::runtime_error("Couldn't read /proc/self/statm");
  std::cout << (boost::format("%1%: %|40t|%2$6.3g GByte process size") % msg % (pages*4096/double(1<<30))) << std::endl;
}

int main(int,char**) {

  tbb::task_scheduler_init tbb_sched;

  const size_t gbyte=(1<<30);
  const size_t S=1*gbyte;     // How many gigabytes we expect to allocate 
  const size_t s=4096;        // Allocated item size
  const size_t n=S/s;         // Number of items to allocate to achieve desired total
  
  typedef std::array<char,s> payload;

  std::vector<payload*> items(n,0);
  tbb::scalable_allocator<payload> tbb_allocator;

  log_process_size("Pre-allocation");
  
  tbb::parallel_for(
    tbb::blocked_range<size_t>(0,items.size()),
    [&items,&tbb_allocator](const tbb::blocked_range<size_t>& r) {
      for (size_t i=r.begin();i<r.end();++i) items[i]=tbb_allocator.allocate(1);
    }
  );
    
  log_process_size("After parallel allocation");
    
  tbb::parallel_for(
    tbb::blocked_range<size_t>(0,items.size()),
    [&items,&tbb_allocator](const tbb::blocked_range<size_t>& r) {
      for (size_t i=r.begin();i<r.end();++i) {tbb_allocator.deallocate(items[i],1);items[i]=0;}
    }
  );
    
  log_process_size("After parallel deallocation"); 

  const int ok=scalable_allocation_command(TBBMALLOC_CLEAN_ALL_BUFFERS,0);
  if (ok!=TBBMALLOC_OK) throw std::logic_error("tbbmalloc clean failed"); 
 
  log_process_size("After tbbmalloc clean");

  return 0;
}
