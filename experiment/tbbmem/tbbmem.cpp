#include <array>
#include <boost/format.hpp>
#include <fstream>
#include <iostream>
#include <tbb/scalable_allocator.h>
#include <tbb/parallel_for.h>
#include <tbb/parallel_invoke.h>
#include <tbb/parallel_reduce.h>
#include <tbb/task_scheduler_init.h>

void log_process_size(const std::string& msg) {
  std::ifstream in("/proc/self/statm");
  size_t pages(0);
  in >> pages;
  if (!in) throw std::runtime_error("Couldn't read /proc/self/statm");
  std::cout << (boost::format("%1%: %|64t|%2$6.3g GByte process size") % msg % (pages*4096/double(1<<30))) << std::endl;
}

void tbb_flush() {
  const int err=scalable_allocation_command(TBBMALLOC_CLEAN_ALL_BUFFERS,0);
  if (err!=TBBMALLOC_OK) throw std::logic_error("TBB flush failed"); 
}

int main(int,char**) {

  tbb::task_scheduler_init tbb_sched;

  const size_t gbyte=(1<<30);
  const size_t S=4*gbyte;
  const size_t s=4096;
  const size_t n=S/s;
  
  typedef std::array<char,s> payload;

  ////////////////
  {
    std::vector<payload*> items(n,0);
    std::allocator<payload> allocator;
   
    log_process_size("serial std allocator");
    
    for (size_t i=0;i<items.size();++i) {
      items[i]=allocator.allocate(1);
    }
    
    log_process_size("  Done allocation");
    
    for (size_t i=0;i<items.size();++i) {
      allocator.deallocate(items[i],1);
      items[i]=0;
    }
    
    log_process_size("  Done deallocation");  
  }

  ////////////////
  {
    std::vector<payload*> items(n,0);
    tbb::scalable_allocator<payload> tbb_allocator;
    log_process_size("serial tbb allocator");

    for (size_t i=0;i<items.size();++i) {
      items[i]=tbb_allocator.allocate(1);
    }
    
    log_process_size("  Done allocation");
    
    for (size_t i=0;i<items.size();++i) {
      tbb_allocator.deallocate(items[i],1);
      items[i]=0;
    }
  
    log_process_size("  Done deallocation");
    tbb_flush();
    log_process_size("  Done tbb_flush"); 
 }

  ////////////////
  {
    std::vector<payload*> items(n,0);
    tbb::scalable_allocator<payload> tbb_allocator;
    log_process_size("parallel_invoke tbb allocator tight");

    tbb::parallel_invoke(
      [&items,&tbb_allocator]() {
        for (size_t i=0;i<items.size()/2;++i) items[i]=tbb_allocator.allocate(1);
        for (size_t i=0;i<items.size()/2;++i) {tbb_allocator.deallocate(items[i],1);items[i]=0;}
      },
      [&items,&tbb_allocator]() {
        for (size_t i=items.size()/2;i<items.size();++i) items[i]=tbb_allocator.allocate(1);
        for (size_t i=items.size()/2;i<items.size();++i) {tbb_allocator.deallocate(items[i],1);items[i]=0;}
      }
    );
    
    log_process_size("  Done allocation deallocation"); 
    tbb_flush();
    log_process_size("  Done tbb_flush"); 
  }


  ////////////////
  {
    std::vector<payload*> items(n,0);
    tbb::scalable_allocator<payload> tbb_allocator;
    log_process_size("parallel_invoke tbb allocator loose");
    
    tbb::parallel_invoke(
      [&items,&tbb_allocator]() {
        for (size_t i=0;i<items.size()/2;++i) items[i]=tbb_allocator.allocate(1);
      },
      [&items,&tbb_allocator]() {
        for (size_t i=items.size()/2;i<items.size();++i) items[i]=tbb_allocator.allocate(1);
      }
    );

    log_process_size("  Done allocation"); 
    
    tbb::parallel_invoke(
      [&items,&tbb_allocator]() {
        for (size_t i=0;i<items.size()/2;++i) {tbb_allocator.deallocate(items[i],1);items[i]=0;}
      },
      [&items,&tbb_allocator]() {
        for (size_t i=items.size()/2;i<items.size();++i) {tbb_allocator.deallocate(items[i],1);items[i]=0;}
      }
    );
    
    log_process_size("  Done deallocation"); 
    tbb_flush();
    log_process_size("  Done tbb_flush"); 
  }

  ////////////////
  {
    log_process_size("parallel_for tbb allocator tight");
    
    std::vector<payload*> items(n,0);
    tbb::scalable_allocator<payload> tbb_allocator;

    log_process_size("  Done setup");
    
    tbb::parallel_for(
      tbb::blocked_range<size_t>(0,items.size()),
      [&items,&tbb_allocator](const tbb::blocked_range<size_t>& r) {
        for (size_t i=r.begin();i<r.end();++i) items[i]=tbb_allocator.allocate(1);
        for (size_t i=r.begin();i<r.end();++i) {tbb_allocator.deallocate(items[i],1);items[i]=0;}
      }
    );
    
    log_process_size("  Done allocation deallocation"); 
    tbb_flush();
    log_process_size("  Done tbb_flush"); 
  }

  ////////////////
  {
    log_process_size("parallel_for tbb allocator tight");
    
    std::vector<payload*> items(n,0);
    tbb::scalable_allocator<payload> tbb_allocator;
  
    log_process_size("  Done setup");
    
    tbb::parallel_for(
      tbb::blocked_range<size_t>(0,items.size()),
      [&items,&tbb_allocator](const tbb::blocked_range<size_t>& r) {
        for (size_t i=r.begin();i<r.end();++i) items[i]=tbb_allocator.allocate(1);
      }
    );
    
    log_process_size("  Done allocation");
    
    tbb::parallel_for(
      tbb::blocked_range<size_t>(0,items.size()),
      [&items,&tbb_allocator](const tbb::blocked_range<size_t>& r) {
        for (size_t i=r.begin();i<r.end();++i) {tbb_allocator.deallocate(items[i],1);items[i]=0;}
      }
    );
    
    log_process_size("  Done deallocation"); 
    tbb_flush();
    log_process_size("  Done tbb_flush"); 
  }

  ////////////////
  {
    log_process_size("serial tbb allocator");

    std::vector<payload*> items(n,0);
    tbb::scalable_allocator<payload> tbb_allocator;
    
    log_process_size("  Done setup");
    
    for (size_t i=0;i<items.size();++i) {
      items[i]=tbb_allocator.allocate(1);
    }
    
    log_process_size("  Done allocation");
    
    for (size_t i=0;i<items.size();++i) {
      tbb_allocator.deallocate(items[i],1);
      items[i]=0;
    }
    
    log_process_size("  Done deallocation"); 
    tbb_flush();
    log_process_size("  Done tbb_flush"); 
  }

  ////////////////
  {
    log_process_size("serial std allocator");
    
    std::vector<payload*> items(n,0);
    std::allocator<payload> std_allocator;
    
    log_process_size("  Done setup");
    
    for (size_t i=0;i<items.size();++i) {
      items[i]=std_allocator.allocate(1);
    }
    
    log_process_size("  Done allocation");
    
    for (size_t i=0;i<items.size();++i) {
      std_allocator.deallocate(items[i],1);
      items[i]=0;
    }
    
    log_process_size("  Done deallocation");  
    tbb_flush();
    log_process_size("  Done tbb_flush"); 
  }

  return 0;
}
