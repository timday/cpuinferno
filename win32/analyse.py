#!/usr/bin/python

def breakup(line):
    assert len(line)>0
    assert line[0]=='$'
    description=line[1:].split(':')[0].strip()
    description=description.replace('tbb parallel_for page strided read random','tbb parallel_reduce page strided read random')  # Fixup bad test name in old runs
    value=float(line[1:].split(':')[1].strip().split(' ')[0])
    units=' '.join(line[1:].split(':')[1].strip().split(' ')[1:]).split('(')[0].strip()
    seconds=float(' '.join(line[1:].split(':')[1].strip().split(' ')[1:]).split('(')[1].split('s')[0])
    return (description,value,units,seconds)

order=[breakup(line)[0] for line in open('results/TDAY1-0.txt') if len(line)>0 and line[0]=='$']

units={breakup(line)[0]:breakup(line)[2] for line in open('results/TDAY1-0.txt') if len(line)>0 and line[0]=='$'}

def results(filename):
    with open(filename) as file:
        return {breakup(line)[0]:(breakup(line)[1],breakup(line)[3]) for line in file if len(line)>0 and line[0]=='$'}

def medianresults(filenames):
    s=map(results,filenames)
    r={}
    for k in order:
        values=sorted(map(lambda x: x[k],s))
        r[k]=values[len(values)/2]
    return r

ref=medianresults(['results/TDAY1-0.txt','results/TDAY1-1.txt','results/TDAY1-2.txt','results/TDAY1-3.txt','results/TDAY1-4.txt','results/TDAY1-5.txt','results/TDAY1-6.txt','results/TDAY1-7.txt','results/TDAY1-8.txt'])

visvmperf_e52660=medianresults(['results/visvmperf_e52660-0.txt','results/visvmperf_e52660-1.txt','results/visvmperf_e52660-2.txt','results/visvmperf_e52660-3.txt','results/visvmperf_e52660-4.txt'])
visvmperf_x5690=medianresults(['results/visvmperf_x5690-0.txt','results/visvmperf_x5690-1.txt','results/visvmperf_x5690-2.txt','results/visvmperf_x5690-3.txt','results/visvmperf_x5690-4.txt'])
visvmperf56903=medianresults(['results/visvmperf56903-0.txt','results/visvmperf56903-1.txt','results/visvmperf56903-2.txt','results/visvmperf56903-3.txt','results/visvmperf56903-4.txt'])
ec2=results('results/ec2-c3.xlarge-e5_2680-2.8GHz.txt')

print '{0:64}     {1:32s}                 {2:10s}  {3:10s}  {4:10s}  {5:10s}'.format('CPUINFERNO','NativeWin7','ESX 2008','ESX 2008' ,'ESX 2003' ,'EC2 2008')
print '{0:64}     {1:32s}                 {2:10s}  {3:10s}  {4:10s}  {5:10s}'.format('TEST'      ,'e5-2643'   ,'e5-2660' ,'x5690'    ,'x5690'    ,'e5-2680')
print '{0:64}     {1:32s}                 {2:10s}  {3:10s}  {4:10s}  {5:10s}'.format('CASE'      ,'2x4@3.3GHz','4@2.2GHz','4@3.47GHz','4@3.47GHz','4@2.8GHz')
for k in order:

    vm0=float(visvmperf_e52660[k][0])/ref[k][0]
    vm1=float(visvmperf_x5690[k][0])/ref[k][0]
    vm2=float(visvmperf56903[k][0])/ref[k][0]
    vm3=float(ec2[k][0])/ref[k][0]
    
    print '{0:64s} {1:6.3g} {2:32s} {3:6g}ms {4:+10.1f}% {5:+10.1f}% {6:+10.1f}% {7:+10.1f}%'.format(
        k,
        ref[k][0],
        units[k],
        1000.0*ref[k][1],
        100.0*vm0-100.0,
        100.0*vm1-100.0,
        100.0*vm2-100.0,
        100.0*vm3-100.0
        )
