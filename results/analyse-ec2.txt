Reference is c1.xlarge PVM results (results/ec2-c1.xlarge-pvm-E5506.txt)
CPUINFERNO                                                           EC2 PVM     EC2 HVM     Hugepages 
TEST                                                                 c3.2xlarge  c3.2xlarge  c3.2xlarge
CASE                                                                 vs.ref      vs.ref      vs.ref    
hailstone numbers                                                     +56.1%      +56.7%      +56.7%
tbb parallel_reduce hailstone numbers (x1 threads)                    +53.9%      +54.2%      +54.2%
tbb parallel_reduce hailstone numbers (x2 threads)                    +54.7%      +54.7%      +54.7%
tbb parallel_reduce hailstone numbers (x3 threads)                    +53.6%      +53.6%      +53.6%
tbb parallel_reduce hailstone numbers (x4 threads)                    +54.4%      +54.4%      +48.3%
tbb parallel_reduce hailstone numbers (x5 threads)                    +45.9%      +46.4%      +46.4%
tbb parallel_reduce hailstone numbers (x6 threads)                    +38.4%      +37.0%      +38.4%
tbb parallel_reduce hailstone numbers (x7 threads)                    +37.1%      +35.9%      +37.5%
tbb parallel_reduce hailstone numbers (x8 threads)                    +27.7%      +27.4%      +28.4%
memset                                                                +87.8%      +85.3%      +88.5%
memsearch                                                            +197.3%     +182.6%     +182.6%
memcpy                                                                +40.9%      +41.8%      +40.9%
tbb parallel_for memset (x1 threads)                                  +85.8%      +83.4%      +86.5%
tbb parallel_for memset (x2 threads)                                  +87.9%      +84.4%      +87.9%
tbb parallel_for memset (x3 threads)                                 +198.5%     +196.6%     +200.4%
tbb parallel_for memset (x4 threads)                                 +257.8%     +252.5%     +257.8%
tbb parallel_for memset (x5 threads)                                 +231.1%     +242.6%     +232.8%
tbb parallel_for memset (x6 threads)                                 +224.6%     +236.1%     +221.4%
tbb parallel_for memset (x7 threads)                                 +220.3%     +238.2%     +249.7%
tbb parallel_for memset (x8 threads)                                 +225.9%     +239.6%     +222.5%
tbb parallel_reduce memsearch (x1 threads)                           +209.5%     +199.2%     +204.3%
tbb parallel_reduce memsearch (x2 threads)                           +230.4%     +226.0%     +243.6%
tbb parallel_reduce memsearch (x3 threads)                           +467.6%     +444.5%     +490.7%
tbb parallel_reduce memsearch (x4 threads)                           +423.5%     +390.3%     +431.9%
tbb parallel_reduce memsearch (x5 threads)                           +490.4%     +482.7%     +513.6%
tbb parallel_reduce memsearch (x6 threads)                           +424.7%     +422.0%     +438.1%
tbb parallel_reduce memsearch (x7 threads)                           +439.4%     +438.1%     +448.7%
tbb parallel_reduce memsearch (x8 threads)                           +452.0%     +453.3%     +464.1%
tbb parallel_for memcpy (x1 threads)                                  +33.8%      +34.6%      +33.8%
tbb parallel_for memcpy (x2 threads)                                  +79.1%      +90.4%      +91.9%
tbb parallel_for memcpy (x3 threads)                                 +170.1%     +223.3%     +217.2%
tbb parallel_for memcpy (x4 threads)                                 +234.3%     +266.3%     +254.7%
tbb parallel_for memcpy (x5 threads)                                 +294.1%     +303.9%     +294.1%
tbb parallel_for memcpy (x6 threads)                                 +332.0%     +298.0%     +298.0%
tbb parallel_for memcpy (x7 threads)                                 +324.4%     +279.4%     +269.8%
tbb parallel_for memcpy (x8 threads)                                 +331.7%     +274.6%     +265.1%
page strided write sequential                                         +94.7%      +19.9%     +116.9%
page strided read sequential                                         +109.4%      +10.3%     +151.2%
page strided write random                                            +200.0%     +108.9%     +331.5%
page strided read random                                             +193.0%     +110.9%     +407.0%
tbb parallel_for page strided write sequential (x1 threads)          +102.3%      +59.6%     +113.9%
tbb parallel_reduce page strided read sequential (x1 threads)        +105.8%      +37.5%     +142.7%
tbb parallel_for page strided write sequential (x2 threads)           +68.8%      +68.6%      +68.8%
tbb parallel_reduce page strided read sequential (x2 threads)        +114.1%      +48.9%     +118.1%
tbb parallel_for page strided write sequential (x3 threads)           +66.1%      +66.5%      +67.6%
tbb parallel_reduce page strided read sequential (x3 threads)         +69.9%      +68.9%      +69.9%
tbb parallel_for page strided write sequential (x4 threads)           +71.4%      +72.5%      +68.9%
tbb parallel_reduce page strided read sequential (x4 threads)         +73.9%      +76.9%      +72.9%
tbb parallel_for page strided write sequential (x5 threads)           +66.9%      +68.7%      +67.1%
tbb parallel_reduce page strided read sequential (x5 threads)         +75.8%      +77.8%      +76.8%
tbb parallel_for page strided write sequential (x6 threads)           +61.6%      +63.4%      +62.1%
tbb parallel_reduce page strided read sequential (x6 threads)         +75.9%      +77.9%      +77.9%
tbb parallel_for page strided write sequential (x7 threads)           +54.3%      +59.5%      +56.5%
tbb parallel_reduce page strided read sequential (x7 threads)         +67.3%      +70.3%      +71.3%
tbb parallel_for page strided write sequential (x8 threads)           +53.7%      +57.1%      +57.3%
tbb parallel_reduce page strided read sequential (x8 threads)         +75.5%      +76.5%      +75.5%
tbb parallel_for page strided write random (x1 threads)              +196.0%     +108.7%     +321.4%
tbb parallel_for page strided read random (x1 threads)               +190.8%     +103.1%     +402.3%
tbb parallel_for page strided write random (x2 threads)              +133.7%     +106.7%     +143.3%
tbb parallel_for page strided read random (x2 threads)               +178.4%      +92.9%     +364.7%
tbb parallel_for page strided write random (x3 threads)               +87.4%      +87.1%      +94.2%
tbb parallel_for page strided read random (x3 threads)               +180.9%     +109.0%     +299.5%
tbb parallel_for page strided write random (x4 threads)               +73.3%      +76.2%      +73.1%
tbb parallel_for page strided read random (x4 threads)               +182.6%     +116.4%     +224.6%
tbb parallel_for page strided write random (x5 threads)               +64.8%      +67.5%      +66.0%
tbb parallel_for page strided read random (x5 threads)               +133.3%      +78.3%     +175.0%
tbb parallel_for page strided write random (x6 threads)               +63.7%      +66.2%      +65.7%
tbb parallel_for page strided read random (x6 threads)               +113.7%      +61.8%     +150.4%
tbb parallel_for page strided write random (x7 threads)               +60.6%      +62.3%      +58.7%
tbb parallel_for page strided read random (x7 threads)                +89.7%      +43.6%     +116.8%
tbb parallel_for page strided write random (x8 threads)               +59.4%      +62.1%      +58.5%
tbb parallel_for page strided read random (x8 threads)               +139.7%      +81.5%     +179.1%
std allocate 1                                                        +36.1%      +86.9%     +158.2%
std deallocate 1                                                      +61.6%      +61.6%      +62.3%
std allocate 4k                                                       +20.1%     +167.1%    +1705.9%
std deallocate 4k                                                     +40.4%     +314.8%    +1343.4%
tbb parallel_for std allocate 1 (x1 threads)                          +39.8%      +91.5%     +164.4%
tbb parallel_for std deallocate 1 (x1 threads)                        +63.8%      +63.8%      +64.1%
tbb parallel_for std allocate 1 (x2 threads)                          +53.7%      +35.1%      +34.3%
tbb parallel_for std deallocate 1 (x2 threads)                        +69.7%     +162.2%     +158.8%
tbb parallel_for std allocate 1 (x3 threads)                          +59.5%      +11.2%     +356.9%
tbb parallel_for std deallocate 1 (x3 threads)                        +91.7%     +156.0%     +190.8%
tbb parallel_for std allocate 1 (x4 threads)                          +61.8%       -8.6%     +112.7%
tbb parallel_for std deallocate 1 (x4 threads)                       +103.4%      +65.4%     +415.7%
tbb parallel_for std allocate 1 (x5 threads)                          +44.8%      +16.8%      +43.4%
tbb parallel_for std deallocate 1 (x5 threads)                       +252.8%      +39.7%     +131.4%
tbb parallel_for std allocate 1 (x6 threads)                          +34.8%       +0.5%      +88.6%
tbb parallel_for std deallocate 1 (x6 threads)                       +180.2%      +40.5%     +130.6%
tbb parallel_for std allocate 1 (x7 threads)                          +47.5%      -27.0%      +61.0%
tbb parallel_for std deallocate 1 (x7 threads)                        +97.0%      +74.3%      +49.7%
tbb parallel_for std allocate 1 (x8 threads)                           +0.3%      -20.6%      +17.9%
tbb parallel_for std deallocate 1 (x8 threads)                       +114.0%      +19.6%      +55.6%
tbb parallel_for std allocate 4k (x1 threads)                         +22.8%     +188.0%    +3144.8%
tbb parallel_for std deallocate 4k (x1 threads)                       +46.7%     +323.5%    +1354.1%
tbb parallel_for std allocate 4k (x2 threads)                          -7.2%      +29.0%    +5624.0%
tbb parallel_for std deallocate 4k (x2 threads)                       +17.7%     +319.9%     +929.7%
tbb parallel_for std allocate 4k (x3 threads)                         +28.7%      +17.8%    +6058.4%
tbb parallel_for std deallocate 4k (x3 threads)                        +7.9%     +280.7%     +568.4%
tbb parallel_for std allocate 4k (x4 threads)                         +28.2%       +8.7%    +3171.8%
tbb parallel_for std deallocate 4k (x4 threads)                       +25.4%     +341.0%     +296.7%
tbb parallel_for std allocate 4k (x5 threads)                         +28.2%       +5.5%    +2481.8%
tbb parallel_for std deallocate 4k (x5 threads)                       +14.6%     +334.3%     +160.6%
tbb parallel_for std allocate 4k (x6 threads)                         +49.1%       +7.4%    +3224.1%
tbb parallel_for std deallocate 4k (x6 threads)                       +36.3%     +280.0%     +144.4%
tbb parallel_for std allocate 4k (x7 threads)                         +40.9%       +2.6%    +4343.5%
tbb parallel_for std deallocate 4k (x7 threads)                       +24.9%     +173.4%      +87.0%
tbb parallel_for std allocate 4k (x8 threads)                         +39.3%       -9.3%    +2050.0%
tbb parallel_for std deallocate 4k (x8 threads)                       +52.7%     +262.0%     +106.0%
tbb scalable pool allocate 1                                          +59.7%      +72.2%      +81.4%
tbb scalable pool deallocate 1                                        +44.1%      +84.6%     +108.8%
tbb scalable pool allocate 4k                                         +26.8%     +186.4%    +1589.8%
tbb scalable pool deallocate 4k                                       +23.4%     +320.8%    +4289.6%
tbb parallel_for tbb scalable pool allocate 1 (x1 threads)            +61.6%      +73.6%      +83.3%
tbb parallel_for tbb scalable pool deallocate 1 (x1 threads)          +45.3%      +83.6%     +108.0%
tbb parallel_for tbb scalable pool allocate 1 (x2 threads)            +60.7%      +72.2%      +76.5%
tbb parallel_for tbb scalable pool deallocate 1 (x2 threads)          +36.8%      +60.3%      +95.0%
tbb parallel_for tbb scalable pool allocate 1 (x3 threads)            +60.6%      +59.6%      +70.6%
tbb parallel_for tbb scalable pool deallocate 1 (x3 threads)          +52.3%      +34.8%      +96.1%
tbb parallel_for tbb scalable pool allocate 1 (x4 threads)            +68.6%      +66.4%      +77.1%
tbb parallel_for tbb scalable pool deallocate 1 (x4 threads)          +65.9%      +57.7%      +97.4%
tbb parallel_for tbb scalable pool allocate 1 (x5 threads)            +42.1%      +41.6%      +46.6%
tbb parallel_for tbb scalable pool deallocate 1 (x5 threads)          +43.4%      +37.2%      +60.2%
tbb parallel_for tbb scalable pool allocate 1 (x6 threads)            +24.7%      +23.7%      +27.0%
tbb parallel_for tbb scalable pool deallocate 1 (x6 threads)          +29.7%      +10.1%      +26.8%
tbb parallel_for tbb scalable pool allocate 1 (x7 threads)            +15.4%      +13.0%      +16.3%
tbb parallel_for tbb scalable pool deallocate 1 (x7 threads)          +18.2%       +3.1%      +18.2%
tbb parallel_for tbb scalable pool allocate 1 (x8 threads)            +20.6%      +20.2%      +18.5%
tbb parallel_for tbb scalable pool deallocate 1 (x8 threads)          +31.0%      +15.2%      +26.9%
tbb parallel_for tbb scalable pool allocate 4k (x1 threads)           +26.9%     +185.5%    +1548.6%
tbb parallel_for tbb scalable pool deallocate 4k (x1 threads)         +21.0%     +312.1%    +4097.5%
tbb parallel_for tbb scalable pool allocate 4k (x2 threads)           +63.4%     +170.2%     +596.3%
tbb parallel_for tbb scalable pool deallocate 4k (x2 threads)         +54.1%      +78.8%     +431.5%
tbb parallel_for tbb scalable pool allocate 4k (x3 threads)           +93.9%     +132.2%     +368.7%
tbb parallel_for tbb scalable pool deallocate 4k (x3 threads)         +66.3%      +37.3%     +181.0%
tbb parallel_for tbb scalable pool allocate 4k (x4 threads)           +94.2%     +174.1%     +274.8%
tbb parallel_for tbb scalable pool deallocate 4k (x4 threads)         +70.1%      +45.0%     +105.3%
tbb parallel_for tbb scalable pool allocate 4k (x5 threads)           +83.2%     +132.3%     +200.6%
tbb parallel_for tbb scalable pool deallocate 4k (x5 threads)         +55.4%       +7.5%      +38.3%
tbb parallel_for tbb scalable pool allocate 4k (x6 threads)           +76.1%     +103.7%     +168.6%
tbb parallel_for tbb scalable pool deallocate 4k (x6 threads)         +47.8%      -14.5%      +11.6%
tbb parallel_for tbb scalable pool allocate 4k (x7 threads)           +76.2%      +76.7%     +134.2%
tbb parallel_for tbb scalable pool deallocate 4k (x7 threads)         +47.6%      -31.5%       -8.1%
tbb parallel_for tbb scalable pool allocate 4k (x8 threads)           +66.2%      +73.3%     +101.3%
tbb parallel_for tbb scalable pool deallocate 4k (x8 threads)         +29.3%      -39.1%      -31.1%
tbb scalable allocate 1                                               +53.0%      +62.9%      +63.6%
tbb scalable deallocate 1                                             +43.3%      +93.5%      +93.5%
tbb scalable allocate 4k                                              +22.4%     +172.4%     +179.1%
tbb scalable deallocate 4k                                            +23.2%     +404.3%     +410.1%
tbb parallel_for tbb scalable allocate 1 (x1 threads)                 +50.8%      +60.3%      +60.9%
tbb parallel_for tbb scalable deallocate 1 (x1 threads)               +43.9%      +91.6%      +90.7%
tbb parallel_for tbb scalable allocate 1 (x2 threads)                 +54.2%      +63.9%      +64.4%
tbb parallel_for tbb scalable deallocate 1 (x2 threads)               +47.8%      +68.9%      +69.1%
tbb parallel_for tbb scalable allocate 1 (x3 threads)                 +54.5%      +64.8%      +64.8%
tbb parallel_for tbb scalable deallocate 1 (x3 threads)               +47.3%      +44.2%      +44.4%
tbb parallel_for tbb scalable allocate 1 (x4 threads)                 +54.5%      +57.7%      +58.5%
tbb parallel_for tbb scalable deallocate 1 (x4 threads)               +79.1%      +55.7%      +53.5%
tbb parallel_for tbb scalable allocate 1 (x5 threads)                 +39.0%      +43.2%      +41.1%
tbb parallel_for tbb scalable deallocate 1 (x5 threads)               +29.0%      +27.5%      +27.5%
tbb parallel_for tbb scalable allocate 1 (x6 threads)                 +28.9%      +26.6%      +27.2%
tbb parallel_for tbb scalable deallocate 1 (x6 threads)               +29.1%      +26.4%      +19.6%
tbb parallel_for tbb scalable allocate 1 (x7 threads)                 +20.0%      +20.5%      +20.5%
tbb parallel_for tbb scalable deallocate 1 (x7 threads)               +14.5%       +8.1%      +11.0%
tbb parallel_for tbb scalable allocate 1 (x8 threads)                 +17.0%      +17.0%      +23.1%
tbb parallel_for tbb scalable deallocate 1 (x8 threads)                +5.7%       +8.9%       +3.6%
tbb parallel_for tbb scalable allocate 4k (x1 threads)                +25.7%     +181.9%     +186.9%
tbb parallel_for tbb scalable deallocate 4k (x1 threads)              +23.0%     +408.6%     +415.8%
tbb parallel_for tbb scalable allocate 4k (x2 threads)                +54.9%     +183.2%     +187.9%
tbb parallel_for tbb scalable deallocate 4k (x2 threads)              +36.9%      +91.7%      +91.1%
tbb parallel_for tbb scalable allocate 4k (x3 threads)                +53.4%     +149.1%     +153.4%
tbb parallel_for tbb scalable deallocate 4k (x3 threads)              +47.9%      +70.0%      +65.5%
tbb parallel_for tbb scalable allocate 4k (x4 threads)               +427.6%     +166.3%     +171.4%
tbb parallel_for tbb scalable deallocate 4k (x4 threads)            +1194.1%     +952.3%     +965.4%
tbb parallel_for tbb scalable allocate 4k (x5 threads)                +56.4%      +17.9%      -34.7%
tbb parallel_for tbb scalable deallocate 4k (x5 threads)              +53.7%      -57.4%      -52.8%
tbb parallel_for tbb scalable allocate 4k (x6 threads)                +87.6%      -35.3%      -24.1%
tbb parallel_for tbb scalable deallocate 4k (x6 threads)               -9.6%      -63.1%      -52.0%
tbb parallel_for tbb scalable allocate 4k (x7 threads)                +94.9%      -43.7%      -29.3%
tbb parallel_for tbb scalable deallocate 4k (x7 threads)             +230.2%      -19.8%       -2.9%
tbb parallel_for tbb scalable allocate 4k (x8 threads)               +619.9%     +153.1%     +134.4%
tbb parallel_for tbb scalable deallocate 4k (x8 threads)             +128.6%      -30.3%      -49.1%
