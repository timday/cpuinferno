Reference is results/ec2-hi1.4xlarge-hvm-E5620.txt (Westmere)
CPUINFERNO                                                            E5-2680     X5570     
TEST                                                                  SndyBrdg    Nehalem   
CASE                                                                  vs.ref      vs.ref    
hailstone numbers                                                     +39.5%      +23.2%
tbb parallel_reduce hailstone numbers (x1 threads)                    +38.2%      +22.4%
tbb parallel_reduce hailstone numbers (x2 threads)                    +39.0%      +23.2%
tbb parallel_reduce hailstone numbers (x3 threads)                    +38.5%      +23.0%
tbb parallel_reduce hailstone numbers (x4 threads)                    +37.3%      +24.8%
tbb parallel_reduce hailstone numbers (x5 threads)                    +37.1%      +22.8%
tbb parallel_reduce hailstone numbers (x6 threads)                    +38.1%      +25.5%
tbb parallel_reduce hailstone numbers (x7 threads)                    +41.1%      +26.5%
tbb parallel_reduce hailstone numbers (x8 threads)                    +44.9%      +30.4%
tbb parallel_reduce hailstone numbers (x9 threads)                    +42.9%      +22.8%
tbb parallel_reduce hailstone numbers (x10 threads)                   +38.0%      +18.4%
tbb parallel_reduce hailstone numbers (x11 threads)                   +33.9%      +18.5%
tbb parallel_reduce hailstone numbers (x12 threads)                   +42.6%      +25.7%
tbb parallel_reduce hailstone numbers (x13 threads)                   +47.7%      +25.9%
tbb parallel_reduce hailstone numbers (x14 threads)                   +38.2%      +12.4%
tbb parallel_reduce hailstone numbers (x15 threads)                   +41.7%       +4.7%
tbb parallel_reduce hailstone numbers (x16 threads)                   +37.0%      +24.9%
memset                                                                +11.4%       -4.0%
memsearch                                                             +75.1%       -7.0%
memcpy                                                                 -4.9%       +8.6%
tbb parallel_for memset (x1 threads)                                  +12.8%       -2.6%
tbb parallel_for memset (x2 threads)                                  +12.2%       -3.1%
tbb parallel_for memset (x3 threads)                                  +53.5%       +3.0%
tbb parallel_for memset (x4 threads)                                  +83.6%       +9.1%
tbb parallel_for memset (x5 threads)                                 +104.2%      +12.6%
tbb parallel_for memset (x6 threads)                                 +113.2%      +16.3%
tbb parallel_for memset (x7 threads)                                 +109.5%      +18.2%
tbb parallel_for memset (x8 threads)                                 +106.8%      +15.8%
tbb parallel_for memset (x9 threads)                                  +81.1%      +20.3%
tbb parallel_for memset (x10 threads)                                 +60.0%      +21.4%
tbb parallel_for memset (x11 threads)                                 +38.5%      +16.9%
tbb parallel_for memset (x12 threads)                                 +35.1%      +18.9%
tbb parallel_for memset (x13 threads)                                 +35.6%      +15.4%
tbb parallel_for memset (x14 threads)                                 +51.0%      +19.0%
tbb parallel_for memset (x15 threads)                                 +41.2%      +12.8%
tbb parallel_for memset (x16 threads)                                 +60.3%      +20.6%
tbb parallel_reduce memsearch (x1 threads)                            +76.6%       -6.4%
tbb parallel_reduce memsearch (x2 threads)                            +75.4%       -5.6%
tbb parallel_reduce memsearch (x3 threads)                           +107.3%       +2.6%
tbb parallel_reduce memsearch (x4 threads)                           +116.7%       +9.8%
tbb parallel_reduce memsearch (x5 threads)                           +108.7%      +11.7%
tbb parallel_reduce memsearch (x6 threads)                            +94.9%      +12.9%
tbb parallel_reduce memsearch (x7 threads)                            +92.0%      +14.7%
tbb parallel_reduce memsearch (x8 threads)                            +90.4%      +12.7%
tbb parallel_reduce memsearch (x9 threads)                            +81.6%       +8.8%
tbb parallel_reduce memsearch (x10 threads)                           +83.8%      +17.0%
tbb parallel_reduce memsearch (x11 threads)                           +80.0%      +14.2%
tbb parallel_reduce memsearch (x12 threads)                           +79.7%      +14.1%
tbb parallel_reduce memsearch (x13 threads)                           +79.6%      +14.6%
tbb parallel_reduce memsearch (x14 threads)                           +77.3%      +14.5%
tbb parallel_reduce memsearch (x15 threads)                           +76.4%      +13.2%
tbb parallel_reduce memsearch (x16 threads)                           +74.1%      +11.5%
tbb parallel_for memcpy (x1 threads)                                   +1.3%      +14.0%
tbb parallel_for memcpy (x2 threads)                                   +0.0%      +10.2%
tbb parallel_for memcpy (x3 threads)                                  +21.6%      +10.1%
tbb parallel_for memcpy (x4 threads)                                  +27.6%       +9.2%
tbb parallel_for memcpy (x5 threads)                                  +36.1%      +10.9%
tbb parallel_for memcpy (x6 threads)                                  +35.3%      +14.2%
tbb parallel_for memcpy (x7 threads)                                  +31.7%      +10.6%
tbb parallel_for memcpy (x8 threads)                                  +32.0%      +13.6%
tbb parallel_for memcpy (x9 threads)                                  +29.1%      +10.7%
tbb parallel_for memcpy (x10 threads)                                 +25.5%       +9.8%
tbb parallel_for memcpy (x11 threads)                                 +27.9%       +9.8%
tbb parallel_for memcpy (x12 threads)                                 +26.2%      +10.8%
tbb parallel_for memcpy (x13 threads)                                 +25.8%       +9.4%
tbb parallel_for memcpy (x14 threads)                                 +26.9%      +10.4%
tbb parallel_for memcpy (x15 threads)                                 +24.1%       +9.9%
tbb parallel_for memcpy (x16 threads)                                 +26.9%      +11.6%
page strided write sequential                                          +9.3%      +34.6%
page strided read sequential                                          +13.3%      +21.2%
page strided write random                                             +48.4%       +6.9%
page strided read random                                              +45.2%       +4.6%
tbb parallel_for page strided write sequential (x1 threads)            -9.8%      +18.3%
tbb parallel_reduce page strided read sequential (x1 threads)         +15.7%      +21.5%
tbb parallel_for page strided write sequential (x2 threads)           -31.6%      +11.1%
tbb parallel_reduce page strided read sequential (x2 threads)          +1.7%      +10.3%
tbb parallel_for page strided write sequential (x3 threads)           -41.9%      +13.8%
tbb parallel_reduce page strided read sequential (x3 threads)          +2.5%      +16.0%
tbb parallel_for page strided write sequential (x4 threads)           -41.6%      +20.6%
tbb parallel_reduce page strided read sequential (x4 threads)         -18.5%       +7.9%
tbb parallel_for page strided write sequential (x5 threads)           -42.0%      +21.5%
tbb parallel_reduce page strided read sequential (x5 threads)         -30.1%       -7.0%
tbb parallel_for page strided write sequential (x6 threads)           -45.2%      +23.1%
tbb parallel_reduce page strided read sequential (x6 threads)         -33.6%       +1.5%
tbb parallel_for page strided write sequential (x7 threads)           -46.0%      +23.3%
tbb parallel_reduce page strided read sequential (x7 threads)         -32.0%      +10.0%
tbb parallel_for page strided write sequential (x8 threads)           -44.6%      +24.4%
tbb parallel_reduce page strided read sequential (x8 threads)         -31.2%      +10.5%
tbb parallel_for page strided write sequential (x9 threads)           -44.0%      +24.4%
tbb parallel_reduce page strided read sequential (x9 threads)         -33.7%       +8.3%
tbb parallel_for page strided write sequential (x10 threads)          -42.5%      +24.8%
tbb parallel_reduce page strided read sequential (x10 threads)        -33.0%       +5.1%
tbb parallel_for page strided write sequential (x11 threads)          -43.6%      +21.9%
tbb parallel_reduce page strided read sequential (x11 threads)        -34.5%       +0.7%
tbb parallel_for page strided write sequential (x12 threads)          -43.3%      +22.8%
tbb parallel_reduce page strided read sequential (x12 threads)        -34.1%       +1.4%
tbb parallel_for page strided write sequential (x13 threads)          -43.6%      +22.0%
tbb parallel_reduce page strided read sequential (x13 threads)        -33.7%       +0.4%
tbb parallel_for page strided write sequential (x14 threads)          -43.5%      +23.0%
tbb parallel_reduce page strided read sequential (x14 threads)        -33.8%       -0.4%
tbb parallel_for page strided write sequential (x15 threads)          -43.5%      +22.2%
tbb parallel_reduce page strided read sequential (x15 threads)        -32.3%       +0.0%
tbb parallel_for page strided write sequential (x16 threads)          -43.7%      +22.2%
tbb parallel_reduce page strided read sequential (x16 threads)        -32.5%       -0.4%
tbb parallel_for page strided write random (x1 threads)               +42.9%       +3.1%
tbb parallel_for page strided read random (x1 threads)                +42.8%       +1.0%
tbb parallel_for page strided write random (x2 threads)               +38.3%       +9.7%
tbb parallel_for page strided read random (x2 threads)                +47.7%      +11.9%
tbb parallel_for page strided write random (x3 threads)               +11.4%       +7.9%
tbb parallel_for page strided read random (x3 threads)                +44.4%       +7.8%
tbb parallel_for page strided write random (x4 threads)               -11.6%       +7.8%
tbb parallel_for page strided read random (x4 threads)                +45.1%       +8.5%
tbb parallel_for page strided write random (x5 threads)               -24.1%      +11.0%
tbb parallel_for page strided read random (x5 threads)                +51.4%      +13.3%
tbb parallel_for page strided write random (x6 threads)               -33.6%      +10.7%
tbb parallel_for page strided read random (x6 threads)                +40.5%       +6.0%
tbb parallel_for page strided write random (x7 threads)               -40.9%      +12.9%
tbb parallel_for page strided read random (x7 threads)                +27.6%       +7.5%
tbb parallel_for page strided write random (x8 threads)               -42.5%      +10.0%
tbb parallel_for page strided read random (x8 threads)                +13.2%       +6.6%
tbb parallel_for page strided write random (x9 threads)               -42.8%       +9.2%
tbb parallel_for page strided read random (x9 threads)                 +9.9%       +6.6%
tbb parallel_for page strided write random (x10 threads)              -43.8%       +6.7%
tbb parallel_for page strided read random (x10 threads)               +14.8%       +8.1%
tbb parallel_for page strided write random (x11 threads)              -42.8%      +10.8%
tbb parallel_for page strided read random (x11 threads)               +16.2%       +8.1%
tbb parallel_for page strided write random (x12 threads)              -41.7%      +11.0%
tbb parallel_for page strided read random (x12 threads)               +20.4%      +11.3%
tbb parallel_for page strided write random (x13 threads)              -42.8%      +10.8%
tbb parallel_for page strided read random (x13 threads)               +17.8%       +6.8%
tbb parallel_for page strided write random (x14 threads)              -42.7%       +8.3%
tbb parallel_for page strided read random (x14 threads)               +18.6%       +8.3%
tbb parallel_for page strided write random (x15 threads)              -38.7%      +19.6%
tbb parallel_for page strided read random (x15 threads)               +42.1%      +29.8%
tbb parallel_for page strided write random (x16 threads)              -33.5%      +30.1%
tbb parallel_for page strided read random (x16 threads)               +41.7%      +30.0%
std allocate 1                                                        +31.8%      +26.0%
std deallocate 1                                                      +44.5%      +22.3%
std allocate 4k                                                       +13.5%      +21.2%
std deallocate 4k                                                     +33.3%      +22.1%
tbb parallel_for std allocate 1 (x1 threads)                          +32.9%      +21.2%
tbb parallel_for std deallocate 1 (x1 threads)                        +46.7%      +24.1%
tbb parallel_for std allocate 1 (x2 threads)                          +72.5%      +46.0%
tbb parallel_for std deallocate 1 (x2 threads)                       +190.4%     +101.9%
tbb parallel_for std allocate 1 (x3 threads)                         +257.9%     +194.4%
tbb parallel_for std deallocate 1 (x3 threads)                       +150.0%      -24.0%
tbb parallel_for std allocate 1 (x4 threads)                         +444.1%     +369.0%
tbb parallel_for std deallocate 1 (x4 threads)                       +174.2%      +61.3%
tbb parallel_for std allocate 1 (x5 threads)                         +415.1%     +344.3%
tbb parallel_for std deallocate 1 (x5 threads)                         +5.7%      -33.0%
tbb parallel_for std allocate 1 (x6 threads)                         +105.2%      +43.2%
tbb parallel_for std deallocate 1 (x6 threads)                       +195.4%      +79.9%
tbb parallel_for std allocate 1 (x7 threads)                         +103.0%     +102.2%
tbb parallel_for std deallocate 1 (x7 threads)                       +227.3%     +112.2%
tbb parallel_for std allocate 1 (x8 threads)                         +161.0%     +129.9%
tbb parallel_for std deallocate 1 (x8 threads)                       +162.6%      +61.7%
tbb parallel_for std allocate 1 (x9 threads)                         +221.2%     +132.0%
tbb parallel_for std deallocate 1 (x9 threads)                       +155.9%     +128.7%
tbb parallel_for std allocate 1 (x10 threads)                        +120.6%     +106.6%
tbb parallel_for std deallocate 1 (x10 threads)                      +193.7%     +138.7%
tbb parallel_for std allocate 1 (x11 threads)                        +149.5%     +124.6%
tbb parallel_for std deallocate 1 (x11 threads)                      +113.5%      +50.3%
tbb parallel_for std allocate 1 (x12 threads)                        +128.3%      +85.3%
tbb parallel_for std deallocate 1 (x12 threads)                       +87.0%      +69.7%
tbb parallel_for std allocate 1 (x13 threads)                        +126.0%     +115.8%
tbb parallel_for std deallocate 1 (x13 threads)                      +127.7%      +52.9%
tbb parallel_for std allocate 1 (x14 threads)                        +110.9%      +63.3%
tbb parallel_for std deallocate 1 (x14 threads)                      +144.5%      +57.5%
tbb parallel_for std allocate 1 (x15 threads)                        +156.4%     +164.5%
tbb parallel_for std deallocate 1 (x15 threads)                       +92.0%     +114.8%
tbb parallel_for std allocate 1 (x16 threads)                        +110.0%      +85.3%
tbb parallel_for std deallocate 1 (x16 threads)                      +136.2%      +24.8%
tbb parallel_for std allocate 4k (x1 threads)                         +15.4%      +21.1%
tbb parallel_for std deallocate 4k (x1 threads)                       +32.9%      +18.4%
tbb parallel_for std allocate 4k (x2 threads)                         -38.4%      -60.3%
tbb parallel_for std deallocate 4k (x2 threads)                       +68.4%      +47.6%
tbb parallel_for std allocate 4k (x3 threads)                        +112.5%     +137.8%
tbb parallel_for std deallocate 4k (x3 threads)                       +23.2%       +2.9%
tbb parallel_for std allocate 4k (x4 threads)                        +178.3%     +141.4%
tbb parallel_for std deallocate 4k (x4 threads)                        +9.6%       +0.5%
tbb parallel_for std allocate 4k (x5 threads)                        +167.5%      +77.1%
tbb parallel_for std deallocate 4k (x5 threads)                       +41.2%      -10.4%
tbb parallel_for std allocate 4k (x6 threads)                        +257.3%     +206.7%
tbb parallel_for std deallocate 4k (x6 threads)                       +18.8%      +12.1%
tbb parallel_for std allocate 4k (x7 threads)                        +245.1%     +209.8%
tbb parallel_for std deallocate 4k (x7 threads)                      +141.7%      +85.8%
tbb parallel_for std allocate 4k (x8 threads)                        +223.4%     +171.1%
tbb parallel_for std deallocate 4k (x8 threads)                      +187.8%      +65.1%
tbb parallel_for std allocate 4k (x9 threads)                        +221.5%     +148.3%
tbb parallel_for std deallocate 4k (x9 threads)                      +146.7%      +99.6%
tbb parallel_for std allocate 4k (x10 threads)                       +261.0%     +193.7%
tbb parallel_for std deallocate 4k (x10 threads)                     +152.2%       -2.7%
tbb parallel_for std allocate 4k (x11 threads)                       +238.6%     +107.2%
tbb parallel_for std deallocate 4k (x11 threads)                     +188.5%      +34.2%
tbb parallel_for std allocate 4k (x12 threads)                       +228.0%     +237.6%
tbb parallel_for std deallocate 4k (x12 threads)                     +140.1%      +94.3%
tbb parallel_for std allocate 4k (x13 threads)                       +124.1%     +227.6%
tbb parallel_for std deallocate 4k (x13 threads)                     +154.2%      +47.5%
tbb parallel_for std allocate 4k (x14 threads)                       +261.2%     +222.6%
tbb parallel_for std deallocate 4k (x14 threads)                     +109.5%      +32.6%
tbb parallel_for std allocate 4k (x15 threads)                       +278.6%     +221.7%
tbb parallel_for std deallocate 4k (x15 threads)                     +191.4%      +67.1%
tbb parallel_for std allocate 4k (x16 threads)                       +292.3%     +226.1%
tbb parallel_for std deallocate 4k (x16 threads)                     +162.2%      +62.7%
tbb scalable pool allocate 1                                          +35.6%      +10.3%
tbb scalable pool deallocate 1                                        +29.0%      +13.7%
tbb scalable pool allocate 4k                                         +14.7%      +16.0%
tbb scalable pool deallocate 4k                                       +24.3%      +21.7%
tbb parallel_for tbb scalable pool allocate 1 (x1 threads)            +41.9%      +14.4%
tbb parallel_for tbb scalable pool deallocate 1 (x1 threads)          +30.3%      +13.9%
tbb parallel_for tbb scalable pool allocate 1 (x2 threads)            +40.5%      +15.5%
tbb parallel_for tbb scalable pool deallocate 1 (x2 threads)          +36.1%      +42.9%
tbb parallel_for tbb scalable pool allocate 1 (x3 threads)            +43.2%      +14.4%
tbb parallel_for tbb scalable pool deallocate 1 (x3 threads)          +54.7%      +38.2%
tbb parallel_for tbb scalable pool allocate 1 (x4 threads)            +42.9%      +13.7%
tbb parallel_for tbb scalable pool deallocate 1 (x4 threads)          +56.4%      +22.5%
tbb parallel_for tbb scalable pool allocate 1 (x5 threads)            +43.1%      +12.8%
tbb parallel_for tbb scalable pool deallocate 1 (x5 threads)          +71.0%      +31.0%
tbb parallel_for tbb scalable pool allocate 1 (x6 threads)            +66.0%      +14.0%
tbb parallel_for tbb scalable pool deallocate 1 (x6 threads)         +103.8%      +39.0%
tbb parallel_for tbb scalable pool allocate 1 (x7 threads)            +45.1%       -4.1%
tbb parallel_for tbb scalable pool deallocate 1 (x7 threads)          +98.3%      +22.3%
tbb parallel_for tbb scalable pool allocate 1 (x8 threads)            +59.6%      +24.9%
tbb parallel_for tbb scalable pool deallocate 1 (x8 threads)         +105.3%      +45.5%
tbb parallel_for tbb scalable pool allocate 1 (x9 threads)            +47.9%       -5.4%
tbb parallel_for tbb scalable pool deallocate 1 (x9 threads)          +77.0%      +41.0%
tbb parallel_for tbb scalable pool allocate 1 (x10 threads)           +51.6%       +6.0%
tbb parallel_for tbb scalable pool deallocate 1 (x10 threads)        +102.3%      +55.0%
tbb parallel_for tbb scalable pool allocate 1 (x11 threads)           +55.8%      +11.6%
tbb parallel_for tbb scalable pool deallocate 1 (x11 threads)        +141.7%      +36.7%
tbb parallel_for tbb scalable pool allocate 1 (x12 threads)           +58.6%       +0.0%
tbb parallel_for tbb scalable pool deallocate 1 (x12 threads)        +216.1%      +80.5%
tbb parallel_for tbb scalable pool allocate 1 (x13 threads)           +57.6%       -1.5%
tbb parallel_for tbb scalable pool deallocate 1 (x13 threads)        +112.0%      -23.2%
tbb parallel_for tbb scalable pool allocate 1 (x14 threads)           +56.4%      +16.0%
tbb parallel_for tbb scalable pool deallocate 1 (x14 threads)        +113.5%      +91.0%
tbb parallel_for tbb scalable pool allocate 1 (x15 threads)           +59.4%      +14.5%
tbb parallel_for tbb scalable pool deallocate 1 (x15 threads)        +147.1%      +89.4%
tbb parallel_for tbb scalable pool allocate 1 (x16 threads)           +59.6%      +18.3%
tbb parallel_for tbb scalable pool deallocate 1 (x16 threads)        +130.1%      +59.3%
tbb parallel_for tbb scalable pool allocate 4k (x1 threads)           +18.8%      +20.8%
tbb parallel_for tbb scalable pool deallocate 4k (x1 threads)         +24.1%      +22.8%
tbb parallel_for tbb scalable pool allocate 4k (x2 threads)           +69.1%      +40.9%
tbb parallel_for tbb scalable pool deallocate 4k (x2 threads)        +128.9%      +87.3%
tbb parallel_for tbb scalable pool allocate 4k (x3 threads)          +123.2%     +100.0%
tbb parallel_for tbb scalable pool deallocate 4k (x3 threads)        +177.1%     +142.2%
tbb parallel_for tbb scalable pool allocate 4k (x4 threads)          +140.4%      +71.1%
tbb parallel_for tbb scalable pool deallocate 4k (x4 threads)        +214.3%     +153.6%
tbb parallel_for tbb scalable pool allocate 4k (x5 threads)          +169.8%      +88.3%
tbb parallel_for tbb scalable pool deallocate 4k (x5 threads)        +326.8%     +204.9%
tbb parallel_for tbb scalable pool allocate 4k (x6 threads)          +202.6%      +94.7%
tbb parallel_for tbb scalable pool deallocate 4k (x6 threads)        +198.8%     +100.0%
tbb parallel_for tbb scalable pool allocate 4k (x7 threads)          +206.4%      +88.5%
tbb parallel_for tbb scalable pool deallocate 4k (x7 threads)        +207.5%      +93.1%
tbb parallel_for tbb scalable pool allocate 4k (x8 threads)          +369.2%     +194.2%
tbb parallel_for tbb scalable pool deallocate 4k (x8 threads)        +277.2%     +132.5%
tbb parallel_for tbb scalable pool allocate 4k (x9 threads)          +817.6%     +482.4%
tbb parallel_for tbb scalable pool deallocate 4k (x9 threads)        +243.9%     +128.5%
tbb parallel_for tbb scalable pool allocate 4k (x10 threads)         +250.7%     +128.4%
tbb parallel_for tbb scalable pool deallocate 4k (x10 threads)       +217.5%     +123.8%
tbb parallel_for tbb scalable pool allocate 4k (x11 threads)         +202.3%     +124.8%
tbb parallel_for tbb scalable pool deallocate 4k (x11 threads)       +186.7%     +115.0%
tbb parallel_for tbb scalable pool allocate 4k (x12 threads)         +233.6%     +123.1%
tbb parallel_for tbb scalable pool deallocate 4k (x12 threads)       +195.3%      +88.4%
tbb parallel_for tbb scalable pool allocate 4k (x13 threads)         +201.5%     +125.0%
tbb parallel_for tbb scalable pool deallocate 4k (x13 threads)       +146.6%      +89.3%
tbb parallel_for tbb scalable pool allocate 4k (x14 threads)         +349.8%     +254.1%
tbb parallel_for tbb scalable pool deallocate 4k (x14 threads)       +143.3%      +89.0%
tbb parallel_for tbb scalable pool allocate 4k (x15 threads)         +186.3%     +155.0%
tbb parallel_for tbb scalable pool deallocate 4k (x15 threads)       +268.6%     +112.5%
tbb parallel_for tbb scalable pool allocate 4k (x16 threads)         +370.1%     +273.1%
tbb parallel_for tbb scalable pool deallocate 4k (x16 threads)       +111.4%      +50.0%
tbb scalable allocate 1                                               +32.3%      +14.2%
tbb scalable deallocate 1                                             +30.3%      +14.6%
tbb scalable allocate 4k                                              +15.9%      +15.9%
tbb scalable deallocate 4k                                            +32.0%      +21.3%
tbb parallel_for tbb scalable allocate 1 (x1 threads)                 +38.5%      +16.0%
tbb parallel_for tbb scalable deallocate 1 (x1 threads)               +38.3%      +22.0%
tbb parallel_for tbb scalable allocate 1 (x2 threads)                 +39.9%      +16.9%
tbb parallel_for tbb scalable deallocate 1 (x2 threads)               +76.3%      +48.9%
tbb parallel_for tbb scalable allocate 1 (x3 threads)                 +40.8%      +17.5%
tbb parallel_for tbb scalable deallocate 1 (x3 threads)               +74.0%      +49.4%
tbb parallel_for tbb scalable allocate 1 (x4 threads)                 +38.6%      +14.3%
tbb parallel_for tbb scalable deallocate 1 (x4 threads)               +20.5%      -24.1%
tbb parallel_for tbb scalable allocate 1 (x5 threads)                 +40.7%      +15.1%
tbb parallel_for tbb scalable deallocate 1 (x5 threads)               +59.2%      +36.0%
tbb parallel_for tbb scalable allocate 1 (x6 threads)                 +38.6%      +17.8%
tbb parallel_for tbb scalable deallocate 1 (x6 threads)               +38.7%      +10.1%
tbb parallel_for tbb scalable allocate 1 (x7 threads)                 +62.1%      +19.7%
tbb parallel_for tbb scalable deallocate 1 (x7 threads)               +72.4%      +24.5%
tbb parallel_for tbb scalable allocate 1 (x8 threads)                 +42.0%      +16.3%
tbb parallel_for tbb scalable deallocate 1 (x8 threads)               +34.1%      +10.2%
tbb parallel_for tbb scalable allocate 1 (x9 threads)                 +63.1%      +32.2%
tbb parallel_for tbb scalable deallocate 1 (x9 threads)               +43.1%      +11.3%
tbb parallel_for tbb scalable allocate 1 (x10 threads)                +53.1%      +20.0%
tbb parallel_for tbb scalable deallocate 1 (x10 threads)              +49.8%      +19.4%
tbb parallel_for tbb scalable allocate 1 (x11 threads)                +57.4%      +21.9%
tbb parallel_for tbb scalable deallocate 1 (x11 threads)              +62.4%      +27.5%
tbb parallel_for tbb scalable allocate 1 (x12 threads)                +72.4%      +29.2%
tbb parallel_for tbb scalable deallocate 1 (x12 threads)              +49.4%      +16.0%
tbb parallel_for tbb scalable allocate 1 (x13 threads)                +64.7%      +22.3%
tbb parallel_for tbb scalable deallocate 1 (x13 threads)              +51.2%      +12.6%
tbb parallel_for tbb scalable allocate 1 (x14 threads)                +69.4%      +21.8%
tbb parallel_for tbb scalable deallocate 1 (x14 threads)              +75.2%      +33.2%
tbb parallel_for tbb scalable allocate 1 (x15 threads)                +63.0%      +20.7%
tbb parallel_for tbb scalable deallocate 1 (x15 threads)             +108.1%      +51.9%
tbb parallel_for tbb scalable allocate 1 (x16 threads)                +70.3%      +18.6%
tbb parallel_for tbb scalable deallocate 1 (x16 threads)              +61.9%      +14.2%
tbb parallel_for tbb scalable allocate 4k (x1 threads)                +16.4%      +14.4%
tbb parallel_for tbb scalable deallocate 4k (x1 threads)              +34.9%      +22.6%
tbb parallel_for tbb scalable allocate 4k (x2 threads)                +84.4%      +43.0%
tbb parallel_for tbb scalable deallocate 4k (x2 threads)             +163.2%     +106.8%
tbb parallel_for tbb scalable allocate 4k (x3 threads)               +214.2%     +232.6%
tbb parallel_for tbb scalable deallocate 4k (x3 threads)             +168.3%     +132.3%
tbb parallel_for tbb scalable allocate 4k (x4 threads)               +173.7%       -5.6%
tbb parallel_for tbb scalable deallocate 4k (x4 threads)            +3839.4%       +6.9%
tbb parallel_for tbb scalable allocate 4k (x5 threads)               +257.7%      +50.2%
tbb parallel_for tbb scalable deallocate 4k (x5 threads)              +98.5%     +257.9%
tbb parallel_for tbb scalable allocate 4k (x6 threads)               +102.9%      +39.7%
tbb parallel_for tbb scalable deallocate 4k (x6 threads)             +410.9%     +406.6%
tbb parallel_for tbb scalable allocate 4k (x7 threads)                +12.8%       -7.9%
tbb parallel_for tbb scalable deallocate 4k (x7 threads)             +271.0%     +321.3%
tbb parallel_for tbb scalable allocate 4k (x8 threads)                +30.9%      +22.4%
tbb parallel_for tbb scalable deallocate 4k (x8 threads)              -47.0%      -87.8%
tbb parallel_for tbb scalable allocate 4k (x9 threads)                -55.3%      -69.7%
tbb parallel_for tbb scalable deallocate 4k (x9 threads)             +232.0%     +771.9%
tbb parallel_for tbb scalable allocate 4k (x10 threads)               +78.2%      +29.6%
tbb parallel_for tbb scalable deallocate 4k (x10 threads)             -42.2%      -61.1%
tbb parallel_for tbb scalable allocate 4k (x11 threads)               +57.2%      -72.1%
tbb parallel_for tbb scalable deallocate 4k (x11 threads)            +142.6%     +393.6%
tbb parallel_for tbb scalable allocate 4k (x12 threads)               +77.0%      +47.7%
tbb parallel_for tbb scalable deallocate 4k (x12 threads)              +7.9%      -74.9%
tbb parallel_for tbb scalable allocate 4k (x13 threads)               +17.5%      -20.9%
tbb parallel_for tbb scalable deallocate 4k (x13 threads)             +89.4%      -13.3%
tbb parallel_for tbb scalable allocate 4k (x14 threads)               +54.3%      +33.1%
tbb parallel_for tbb scalable deallocate 4k (x14 threads)             +18.2%      +15.0%
tbb parallel_for tbb scalable allocate 4k (x15 threads)              +681.5%     +410.4%
tbb parallel_for tbb scalable deallocate 4k (x15 threads)             +31.3%      -24.9%
tbb parallel_for tbb scalable allocate 4k (x16 threads)               +22.4%       +9.7%
tbb parallel_for tbb scalable deallocate 4k (x16 threads)           +1186.1%     +333.6%
