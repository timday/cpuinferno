#!/usr/bin/python

order=[line[1:].split(':')[0].strip() for line in open('results/namche.txt') if len(line)>0 and line[0]=='$']

def results(filename):
    with open(filename) as file:
        return {line[1:].split(':')[0].strip() : line.split(':')[1].strip().split(' ')[0] for line in file if len(line)>0 and line[0]=='$'}

ref=results('results/namche.txt')
refhuge=results('results/namche-hugepages.txt')

hvm=results('results/ec2-c3.2xlarge-hmv.txt')
hvmhuge=results('results/ec2-c3.2xlarge-hmv-hugepages.txt')
pvm=results('results/ec2-c3.2xlarge-pvm.txt')

c1pvm=results('results/ec2-c1.xlarge-pvm-E5506.txt')

vbox=results('results/namche-vm-debian1.txt')

print '{0:64}     {1:10s}  {2:10s}  {3:10s}  {4:10s}            {5:10s}  {6:10s}'.format('CPUINFERNO','EC2 HVM'   ,'EC2 PVM'   ,'EC2 PVM','Virtualbox','Hugepages','Hugepages' )
print '{0:64}     {1:10s}  {2:10s}  {3:10s}  {4:10s}            {5:10s}  {6:10s}'.format('TEST'      ,'c3.2xlarge','c3.2xlarge','c1.xlarge','4.1.18','morecore','morecore' )
print '{0:64}     {1:10s}  {2:10s}  {3:10s}  {4:10s}            {5:10s}  {6:10s}'.format('CASE'      ,'vs.ref'    ,'vs.ref'    ,'vs.ref','on ref'    ,'on ref'   ,'on EC2 HVM')
for k in order:

    h=float(refhuge[k])/float(ref[k])

    aws0=float(hvm[k])/float(ref[k])
    aws0huge=float(hvmhuge[k])/float(hvm[k])
    aws1=float(pvm[k])/float(ref[k])

    aws2=float(c1pvm[k])/float(ref[k])

    f=float(vbox[k])/float(ref[k])
    d=int(1.0/f)
    alert=''
    while d>1:
        alert+='!'
        d=d/2
    
    print '{0:64} {1:+10.1f}% {2:+10.1f}% {3:+10.1f}% {4:+10.1f}% {5: <8s} {6:+10.1f}% {7:+10.1f}% '.format(
        k,100.0*aws0-100.0,100.0*aws1-100.0,100.0*aws2-100.0,100.0*f-100.0,alert,100.0*h-100,100.0*aws0huge-100.0
        )
