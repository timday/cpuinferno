#include "cpuinferno_functions.h"

#include <boost/test/test_tools.hpp>
#include <emmintrin.h>

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <windows.h>
#endif

char memsum(const char* src,size_t n)
{
  const __m128i* p=reinterpret_cast<const __m128i*>(src);
  __m128i v=_mm_set_epi32(0,0,0,0);

  while (n>=4*sizeof(__m128i)) {
    n-=4*sizeof(__m128i);
    v=_mm_add_epi8(v,p[0]);
    v=_mm_add_epi8(v,p[1]);
    v=_mm_add_epi8(v,p[2]);
    v=_mm_add_epi8(v,p[3]);
    p+=4;
  }

  while (n>=sizeof(__m128i)) {
    n-=sizeof(__m128i);
    v=_mm_add_epi8(v,*p);
    p++;
  }

  char c=0;
  src=reinterpret_cast<const char*>(p);
  while (n>0) {
    n--;
    c+=*src;
    src++;
  }

  const char*const vc=reinterpret_cast<const char*>(&v);
  return c+vc[0]+vc[1]+vc[2]+vc[3]+vc[4]+vc[5]+vc[6]+vc[7]+vc[8]+vc[9]+vc[10]+vc[11]+vc[12]+vc[13]+vc[14]+vc[15];
}

size_t iterate_hailstone(size_t v,size_t n) {
  for (size_t i=0;i<n;++i) {
    v=(v&1 ? 3*v+1 : v/2);
  }
  return v;
}

#ifdef WIN32
void hit_4k_page(size_t i,void*const*const pages,bool& trouble) {
  __try {
    *reinterpret_cast<char*>(pages[i])=1;
  }
  __except (EXCEPTION_EXECUTE_HANDLER) {  // Just assume this is definitely a page fault
    const void*const ptr=VirtualAlloc(pages[i],4096,MEM_COMMIT,PAGE_READWRITE);
    if (ptr==pages[i]) {
      __try {
        *reinterpret_cast<char*>(pages[i])=1;
      }
      __except (EXCEPTION_EXECUTE_HANDLER) {
        trouble=true;
      }
    } else {
      trouble=true;
    }
  }
}
#endif

void touch_4k_pages(const std::vector<void*>& pages,const bool safe) {
#ifdef WIN32
  if (safe) {
    for (size_t i=0;i<pages.size();++i) {
      *reinterpret_cast<char*>(pages[i])=1;
    }
  } else {
    bool trouble=false;
    for (size_t i=0;i<pages.size();++i) {
      hit_4k_page(i,&pages[0],trouble);
    }
    BOOST_CHECK(!trouble);
  }
#else
  for (size_t i=0;i<pages.size();++i) {
    *reinterpret_cast<char*>(pages[i])=1;
  }
#endif
}