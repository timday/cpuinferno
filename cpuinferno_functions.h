#ifndef _cpuinferno_functions_h_
#define _cpuinferno_functions_h_

#include <cstddef>
#include <cstdint>
#include <vector>

// Moved out of cpuinferno.cpp to eliminate possibility of inlining funnies

extern char memsum(const char* src,size_t n);

extern size_t iterate_hailstone(size_t v,size_t n);

extern void hit_4k_page(size_t i,void*const*const pages,bool& trouble);
extern void touch_4k_pages(const std::vector<void*>& pages,const bool safe);

#endif
