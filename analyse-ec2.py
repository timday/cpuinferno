#!/usr/bin/python

ref='results/ec2-c1.xlarge-pvm-E5506.txt' # Or results/ec2-c1.xlarge-pvm-E5410.txt

order=[line[1:].split(':')[0].strip() for line in open(ref) if len(line)>0 and line[0]=='$']

def results(filename):
    with open(filename) as file:
        return {line[1:].split(':')[0].strip() : line.split(':')[1].strip().split(' ')[0] for line in file if len(line)>0 and line[0]=='$'}


c1pvm=results(ref)
c3pvm=results('results/ec2-c3.2xlarge-pvm.txt')
c3hvm=results('results/ec2-c3.2xlarge-hmv.txt')
c3hvmhuge=results('results/ec2-c3.2xlarge-hmv-hugepages.txt')

print 'Reference is c1.xlarge PVM results ({0})'.format(ref)
print '{0:64}     {1:10s}  {2:10s}  {3:10s}'.format('CPUINFERNO','EC2 PVM'   ,'EC2 HVM'   ,'Hugepages')
print '{0:64}     {1:10s}  {2:10s}  {3:10s}'.format('TEST'      ,'c3.2xlarge','c3.2xlarge','c3.2xlarge')
print '{0:64}     {1:10s}  {2:10s}  {3:10s}'.format('CASE'      ,'vs.ref'    ,'vs.ref'    ,'vs.ref')
for k in order:

    ref=c1pvm
    gain0=float(c3pvm[k])/float(ref[k])
    gain1=float(c3hvm[k])/float(ref[k])
    gain2=float(c3hvmhuge[k])/float(ref[k])

    print '{0:64} {1:+10.1f}% {2:+10.1f}% {3:+10.1f}%'.format(
        k,100.0*gain0-100.0,100.0*gain1-100.0,100.0*gain2-100.0
        )
